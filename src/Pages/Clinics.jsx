import React from "react";
import { useState, useEffect } from "react";
import {
    Box,
    Text,
    Heading,
    Spacer,
    Button,
    VStack,
    Input,
    Select,
    HStack,
    Center,
} from "@chakra-ui/react";
import CardGrid from "../Components/CardGrid";
import { InstanceCard } from "../Components/InstanceCard";
import axios from "axios";

function Clinics() {
    const [clinics, setClinics] = useState([]);
    const [searchQuery, setSearchQuery] = useState("");
    const [sortField, setSortField] = useState("");
    const [sortOrder, setSortOrder] = useState("True");
    const [currentPage, setCurrentPage] = useState(1);
    const [county, setCounty] = useState("");
    const [forProfit, setForProfit] = useState("");

    useEffect(() => {
        let url = "https://new-york-aid.me/api/getAllClinics";

        const filterStates = [
            { param: "county", value: county },
            { param: "forProfit", value: forProfit },
            { param: "sortBy", value: sortField },
            { param: "ascending", value: sortOrder },
        ];
        // Build the query string by filtering out empty states and joining them
        const query = filterStates
            .filter((filter) => filter.value)
            .map(
                (filter) =>
                    `${filter.param}=${encodeURIComponent(filter.value)}`
            )
            .join("&");
        if (query) {
            url += `?${query}`;
        }

        axios
            .get(url)
            .then((response) => {
                setClinics(response.data);
            })
            .catch((error) => {
                console.error("Error fetching clinics:", error);
            });
    }, [sortField, sortOrder, county, forProfit]);

    // Create a memoized version of the filtered clinics that updates when `clinics` or `searchQuery` changes
    const filteredClinics = React.useMemo(() => {
        return searchQuery
            ? clinics.filter((clinic) => {
                  const lowerCaseQuery = searchQuery.toLowerCase();
                  // Check if the property exists and is not null before calling toString or toLowerCase
                  return (
                      clinic.facility_name
                          .toLowerCase()
                          .includes(lowerCaseQuery) ||
                      (clinic.full_address &&
                          clinic.full_address
                              .toLowerCase()
                              .includes(lowerCaseQuery)) ||
                      (clinic.county &&
                          clinic.county
                              .toLowerCase()
                              .includes(lowerCaseQuery)) ||
                      (clinic.fac_zip !== null &&
                          clinic.fac_zip.toString().includes(lowerCaseQuery))
                  );
              })
            : clinics;
    }, [clinics, searchQuery]);

    // Pagination logic
    const ITEMS_PER_PAGE = 20;
    const totalPages = Math.ceil(filteredClinics.length / ITEMS_PER_PAGE);
    const startIndex = (currentPage - 1) * ITEMS_PER_PAGE;
    const currentClinics = filteredClinics.slice(
        startIndex,
        startIndex + ITEMS_PER_PAGE
    );

    const handleSearch = (e) => {
        setSearchQuery(e.target.value);
        setCurrentPage(1); // Reset to the first page when search changes
    };

    const getHighlightedText = (text, query) => {
        if (!query) return text;
        const parts = text.split(new RegExp(`(${query})`, "gi"));
        return parts.map((part, index) =>
            part.toLowerCase() === query.toLowerCase() ? (
                <mark key={index}>{part}</mark>
            ) : (
                part
            )
        );
    };

    const renderClinics = () => {
        return currentClinics.map((clinic) => (
            <InstanceCard
                key={clinic.id}
                title={getHighlightedText(clinic.facility_name, searchQuery)}
                align="center"
                link={`/clinic_instance/${clinic.id}/`}
                width="400px"
                type="clinic"
                data={clinic}
                image={clinic.image}
                searchQuery={searchQuery}
            />
        ));
    };

    const maxButtonsToShow = 10;
    const [buttonStartIndex, setButtonStartIndex] = React.useState(0);

    const getButtonRange = () => {
        const start = buttonStartIndex;
        const end = Math.min(buttonStartIndex + maxButtonsToShow, totalPages);
        return { start, end };
    };

    const onNextButtonSet = () => {
        if (buttonStartIndex + maxButtonsToShow < totalPages) {
            setButtonStartIndex(buttonStartIndex + maxButtonsToShow);
        }
    };

    const onPrevButtonSet = () => {
        if (buttonStartIndex > 0) {
            setButtonStartIndex(buttonStartIndex - maxButtonsToShow);
        }
    };

    const { start, end } = getButtonRange();

    return (
        <VStack data-testid="vstack-element" bg="black" spacing={5} pb={5}>
            <Box
                width="100%"
                textAlign="center"
                mt={5}
                position="relative"
                px={4}
            >
                <Heading
                    as="h1"
                    size="2xl"
                    fontWeight="bold"
                    letterSpacing="wider"
                    textTransform="uppercase"
                    color="white"
                >
                    Clinics
                </Heading>
                <Spacer height="20px" />
                <Center>
                    <Text color="white" fontWeight="bold">
                        Number of Clinics: {filteredClinics.length}
                    </Text>
                </Center>
            </Box>
            <Spacer height="20px" />
            <HStack>
                <Input
                    placeholder="Search clinics..."
                    onChange={handleSearch}
                    name="clinic_search"
                    mb="4"
                    data-testid="search-input"
                    bg="white"
                />
                <Select
                    placeholder="Sort By"
                    onChange={(e) => setSortField(e.target.value)}
                    mb="4"
                    data-testid="sort-select"
                    width={"200px"}
                    bg={"white"}
                >
                    <option value="facility_name">Name</option>
                    <option value="zipcode">Zip Code</option>
                    <option value="county">County</option>
                    <option value="phone">Phone Number</option>
                    <option value="website">Website</option>
                </Select>
                <Select
                    placeholder="Order"
                    onChange={(e) => setSortOrder(e.target.value)}
                    mb="4"
                    data-testid="order-select"
                    width={"200px"}
                    bg={"white"}
                >
                    <option value="True">Ascending</option>
                    <option value="False">Descending</option>
                </Select>
                <Select
                    placeholder="County"
                    onChange={(e) => setCounty(e.target.value)}
                    mb="4"
                    data-testid="order-select"
                    width={"400px"}
                    bg={"white"}
                >
                    <option value="New York">New York</option>
                    <option value="Queens">Queens</option>
                    <option value="Kings">Kings</option>
                    <option value="Brooklyn">Brooklyn</option>
                    <option value="Bronx">Bronx</option>
                    <option value="Richmond">Richmond</option>
                </Select>
                <Select
                    placeholder="Ownership Type"
                    onChange={(e) => setForProfit(e.target.value)}
                    mb="4"
                    data-testid="order-select"
                    width={"400px"}
                    bg={"white"}
                >
                    <option value="Business Corporation">Business Corporation</option>
                    <option value="LLC">LLC</option>
                    <option value="Not for Profit Corporation">Not For Profit</option>
                    <option value="Municipality">Municipality</option>
                    <option value="Individual">Individual</option>
                    <option value="County">County</option>
                </Select>
            </HStack>
            <Spacer height="20px" />
            <CardGrid data-testid="card-grid">{renderClinics()}</CardGrid>
            <Box display="flex" justifyContent="center" mt={5} flexWrap="wrap">
                {start > 0 && (
                    <Button onClick={onPrevButtonSet}>Previous</Button>
                )}
                {Array.from({ length: totalPages })
                    .slice(start, end)
                    .map((_, index) => (
                        <Button
                            key={index + start}
                            p={2}
                            mx={1}
                            mt={1}
                            cursor="pointer"
                            borderWidth={2}
                            borderRadius="md"
                            bg={
                                currentPage === index + start + 1
                                    ? "black"
                                    : "white"
                            }
                            color={
                                currentPage === index + start + 1
                                    ? "white"
                                    : "black"
                            }
                            onClick={() => setCurrentPage(index + start + 1)}
                            _hover={{
                                color: "white",
                                bg: "black",
                                transform: "scale(1.05)",
                                transition: "all 0.2s",
                            }}
                        >
                            {index + start + 1}
                        </Button>
                    ))}
                {end < totalPages && (
                    <Button
                        bg={"white"}
                        onClick={onNextButtonSet}
                        _hover={{
                            color: "white",
                            bg: "black",
                            transform: "scale(1.05)",
                            transition: "all 0.2s",
                            border: "2px",
                        }}
                    >
                        Next
                    </Button>
                )}
            </Box>
        </VStack>
    );
}

export default Clinics;
