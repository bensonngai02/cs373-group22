import React from "react";
import { useState, useEffect } from "react";
import CardGrid from "../Components/CardGrid";
import {
    Box,
    Text,
    Heading,
    Spacer,
    Button,
    VStack,
    Input,
    Select,
    HStack,
    Center,
} from "@chakra-ui/react";
import { InstanceCard } from "../Components/InstanceCard";
import axios from "axios";

function Housing() {
    const [housing, setHousing] = useState([]);
    const [searchQuery, setSearchQuery] = useState("");
    const [sortField, setSortField] = useState("projectName");
    const [sortOrder, setSortOrder] = useState("True");
    const [currentPage, setCurrentPage] = useState(1);

    const [incomeClass, setIncomeClass] = useState("");
    const [completionStatus, setCompletionStatus] = useState("");

    useEffect(() => {
        let url = "https://new-york-aid.me/api/getAllHousing";
        const filterStates = [
            { param: "incomeClass", value: incomeClass },
            { param: "isCompleted", value: completionStatus },
            { param: 'sortBy', value: sortField},
            { param: 'ascending', value: sortOrder}
        ];

        // Build the query string by filtering out empty states and joining them
        const query = filterStates
            .filter((filter) => filter.value)
            .map(
                (filter) =>
                    `${filter.param}=${encodeURIComponent(filter.value)}`
            )
            .join("&");
        if (query) {
            url += `?${query}`;
        }
        console.log(url);

        axios
            .get(url)
            .then((response) => {
                setHousing(response.data);
            })
            .catch((error) => {
                console.error("Error fetching housing data:", error);
            });
    }, [sortField, sortOrder, incomeClass, completionStatus]);

    // Create a memoized version of the filtered housing  that updates when `housing` or `searchQuery` changes
    const filteredHousing = React.useMemo(() => {
        return searchQuery
            ? housing.filter((house) => {
                  const lowerCaseQuery = searchQuery.toLowerCase();
                  return (
                      house.project_name
                          .toLowerCase()
                          .includes(lowerCaseQuery) ||
                      house.full_address
                          .toLowerCase()
                          .includes(lowerCaseQuery) ||
                      house.primary_income_class
                          .toLowerCase()
                          .includes(lowerCaseQuery) ||
                      house.zip_code.toString().includes(lowerCaseQuery)
                  );
              })
            : housing;
    }, [housing, searchQuery]);

    // Pagination
    const ITEMS_PER_PAGE = 20;
    const totalPages = Math.ceil(filteredHousing.length / ITEMS_PER_PAGE);
    const startIndex = (currentPage - 1) * ITEMS_PER_PAGE;
    const currentHousing = filteredHousing.slice(
        startIndex,
        startIndex + ITEMS_PER_PAGE
    );

    const handleSearch = (e) => {
        setSearchQuery(e.target.value);
        setCurrentPage(1);
    };

    const getHighlightedText = (text, query) => {
        if (!query) return text;
        const parts = text.split(new RegExp(`(${query})`, "gi"));
        return parts.map((part, index) =>
            part.toLowerCase() === query.toLowerCase() ? (
                <mark key={index}>{part}</mark>
            ) : (
                part
            )
        );
    };

    const renderHousing = () => {
        return currentHousing.map((house) => (
            <InstanceCard
                key={house.id}
                title={getHighlightedText(house.project_name, searchQuery)}
                align="center"
                link={`/housing_instance/${house.id}/`}
                width="400px"
                type="housing"
                data={house}
                searchQuery={searchQuery}
            />
        ));
    };

    const maxButtonsToShow = 10;
    const [buttonStartIndex, setButtonStartIndex] = React.useState(0);

    const getButtonRange = () => {
        const start = buttonStartIndex;
        const end = Math.min(buttonStartIndex + maxButtonsToShow, totalPages);
        return { start, end };
    };

    const onNextButtonSet = () => {
        if (buttonStartIndex + maxButtonsToShow < totalPages) {
            setButtonStartIndex(buttonStartIndex + maxButtonsToShow);
        }
    };

    const onPrevButtonSet = () => {
        if (buttonStartIndex > 0) {
            setButtonStartIndex(buttonStartIndex - maxButtonsToShow);
        }
    };

    const { start, end } = getButtonRange();

    return (
        <VStack bg={"black"} spacing={5} pb={5}>
            <Box
                width="100%"
                textAlign="center"
                mt={5}
                position="relative"
                px={4}
            >
                <Heading
                    as="h1"
                    size="2xl"
                    fontWeight="bold"
                    letterSpacing="wider"
                    textTransform="uppercase"
                    color="white"
                >
                    Housing
                </Heading>
                <Spacer height="20px" />
                <Center>
                    <Text color="white" fontWeight="bold">
                        Number of Low-Cost Housing Buildings:{" "}
                        {filteredHousing.length}
                    </Text>
                </Center>
            </Box>
            <spacer height="20px" />
            <HStack width={"75%"}>
                <Input
                    placeholder="Search housing..."
                    onChange={handleSearch}
                    name="housing_search"
                    mb="4"
                    data-testid="search-input"
                    bg="white"
                />
                <Select
                    placeholder="Sort By"
                    onChange={(e) => setSortField(e.target.value)}
                    mb="4"
                    data-testid="sort-select"
                    width={"40%"}
                    bg={"white"}
                >
                    <option value="projectName">Name</option>
                    <option value="zipcode">Zip Code</option>
                    <option value="incomeClass">Income Class</option>
                    <option value="totalUnits">Total Units</option>
                    <option value="totalBeds">Total Beds</option>
                </Select>
                <Select
                    placeholder="Order"
                    onChange={(e) => setSortOrder(e.target.value)}
                    mb="4"
                    data-testid="order-select"
                    width={"40%"}
                    bg={"white"}
                >
                    <option value="True">Ascending</option>
                    <option value="False">Descending</option>
                </Select>
                {/* Income Class filter */}
                <Select
                    placeholder="Income Class"
                    onChange={(e) => setIncomeClass(e.target.value)}
                    mb="4"
                    bg={"white"}
                    width={"60%"}
                >
                    <option value="Extremely Low Income Units">
                        Extremely Low Income Units
                    </option>
                    <option value="Very Low Income Units">
                        Very Low Income Units
                    </option>
                    <option value="Low Income Units">
                        Low Income Units
                    </option>
                    <option value="Moderate Income Units">
                        Moderate Income Units
                    </option>
                    <option value="Middle Income Units">
                        Middle Income Units
                    </option>
                </Select>
                {/* Completion Status filter */}
                <Select
                    placeholder="Completion Status"
                    onChange={(e) => setCompletionStatus(e.target.value)}
                    mb="4"
                    bg={"white"}
                    width={"60%"}
                >
                    <option value="1">Completed</option>
                    <option value="0">Not Completed</option>
                </Select>
            </HStack>
            <Spacer height="20px" />
            <CardGrid data-testid="card-grid">{renderHousing()}</CardGrid>

            <Box display="flex" justifyContent="center" mt={5} flexWrap="wrap">
                {start > 0 && (
                    <Button onClick={onPrevButtonSet}>Previous</Button>
                )}
                {Array.from({ length: totalPages })
                    .slice(start, end)
                    .map((_, index) => (
                        <Button
                            key={index + start}
                            p={2}
                            mx={1}
                            mt={1}
                            cursor="pointer"
                            borderWidth={2}
                            borderRadius="md"
                            bg={
                                currentPage === index + start + 1
                                    ? "black"
                                    : "white"
                            }
                            color={
                                currentPage === index + start + 1
                                    ? "white"
                                    : "black"
                            }
                            onClick={() => setCurrentPage(index + start + 1)}
                            _hover={{
                                color: "white",
                                bg: "black",
                                transform: "scale(1.05)",
                                transition: "all 0.2s",
                            }}
                        >
                            {index + start + 1}
                        </Button>
                    ))}
                {end < totalPages && (
                    <Button onClick={onNextButtonSet}>Next</Button>
                )}
            </Box>
        </VStack>
    );
}

export default Housing;
