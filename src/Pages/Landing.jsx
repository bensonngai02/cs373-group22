import React, { useEffect } from "react";
import {
    Flex,
    Heading,
    Image,
    Stack,
    Text,
    useDisclosure,
    Box,
} from "@chakra-ui/react";
import RippleText from "../Components/RippleText";

import homelessImage from "../pictures/Homeless.jpg";

function Landing() {
    const { isOpen: isImageOpen, onOpen: openImage } = useDisclosure();
    const { isOpen: isTextOpen, onOpen: openText } = useDisclosure();

    useEffect(() => {
        openImage(); // Open the image fade-in
        openText(); // Open the text fade-in
    }, [openImage, openText]);

    return (
        <Flex minH={"100vh"} direction={{ base: "column", md: "row" }}>
            <Flex
                p={8}
                flex={1}
                align={"center"}
                justify={"center"}
                bgColor={"black"}
                color={"white"}
                paddingBottom={50}
            >
                <Stack
                    spacing={2}
                    w={"full"}
                    maxW={"lg"}
                    opacity={isTextOpen ? 1 : 0}
                    transform={`translateX(${isTextOpen ? "0" : "-50px"})`}
                    transition="opacity 1s, transform 1s"
                >
                    <Heading
                        fontSize={{ base: "4xl", md: "5xl", lg: "6xl" }}
                        mb={0}
                    >
                        <Box>
                            <RippleText
                                as={"span"}
                                position={"relative"}
                                className="font-face-gm"
                                transition="opacity 1s, transform 1s"
                            >
                                New York Aid
                            </RippleText>
                        </Box>
                        <Box mt={-5}>
                            <RippleText
                                fontSize={{ base: "2xl", md: "2xl", lg: "2xl" }}
                                as={"span"}
                                position={"relative"}
                                transition="opacity 1s, transform 1s"
                                opacity={isTextOpen ? 1 : 0}
                            >
                                Illuminating Paths, Changing Lives
                            </RippleText>
                        </Box>
                    </Heading>
                    <Text
                        fontSize={{ base: "lg", lg: "lg" }}
                        color={"white"}
                        transition="opacity 1.5s"
                        opacity={isTextOpen ? 1 : 0}
                    >
                        In the city that never sleeps, thousands sleep on its
                        streets every night. New York Aid is here to change that
                        narrative.
                    </Text>
                </Stack>
            </Flex>
            <Flex
                flex={1}
                opacity={isImageOpen ? 1 : 0}
                transform={`translateY(${isImageOpen ? "0" : "-5px"})`}
                transition="opacity 1s, transform 1s"
                background={"black"}
            >
                <Image
                    alt={"Landing Page Image"}
                    objectFit={"cover"}
                    src={homelessImage}
                    background={"black"}
                />
            </Flex>
        </Flex>
    );
}

export default Landing;
