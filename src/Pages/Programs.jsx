import React from "react";
import { useState, useEffect, useMemo } from "react";
import CardGrid from "../Components/CardGrid";
import {
    Box,
    Text,
    Heading,
    Spacer,
    Button,
    VStack,
    Input,
    Select,
    HStack,
    Center,
} from "@chakra-ui/react";
import { InstanceCard } from "../Components/InstanceCard";
import axios from "axios";

function Programs() {
    const [programs, setPrograms] = useState([]);
    const [searchQuery, setSearchQuery] = useState("");
    const [sortField, setSortField] = useState("programName");
    const [sortOrder, setSortOrder] = useState("True");
    const [currentPage, setCurrentPage] = useState(1);

    const [borough, setBorough] = useState("");
    const [programType, setProgramType] = useState("");

    useEffect(() => {
        const filterStates = [
            { param: "borough", value: borough },
            { param: "type", value: programType },
            { param: 'sortBy', value: sortField},
            { param: 'ascending', value: sortOrder}
        ];

        let url = "https://new-york-aid.me/api/getAllPrograms";

        const query = filterStates
            .filter((filter) => filter.value)
            .map(
                (filter) =>
                    `${filter.param}=${encodeURIComponent(filter.value)}`
            )
            .join("&");
        if (query) {
            url += `?${query}`;
        }

        axios
            .get(url)
            .then((response) => {
                setPrograms(response.data);
            })
            .catch((error) => {
                console.error("Error fetching programs:", error);
            });
    }, [sortField, sortOrder, borough, programType]);

    const filteredPrograms = useMemo(() => {
        return searchQuery
            ? programs.filter((program) => {
                  const lowerCaseQuery = searchQuery.toLowerCase();

                  // Convert all searchable fields to strings and check if they include the search query
                  return (
                      program.program_name
                          .toLowerCase()
                          .includes(lowerCaseQuery) ||
                      (program.address &&
                          program.address
                              .toLowerCase()
                              .includes(lowerCaseQuery)) ||
                      (program.borough &&
                          program.borough
                              .toLowerCase()
                              .includes(lowerCaseQuery)) ||
                      (program.phone &&
                          program.phone
                              .toLowerCase()
                              .includes(lowerCaseQuery)) ||
                      (program.zip_code &&
                          program.zip_code
                              .toString()
                              .includes(lowerCaseQuery)) ||
                      (program.nta &&
                          program.nta.toLowerCase().includes(lowerCaseQuery)) ||
                      (program.type &&
                          program.type.toLowerCase().includes(lowerCaseQuery))
                  );
              })
            : programs;
    }, [programs, searchQuery]);

    const ITEMS_PER_PAGE = 20;
    const totalPages = Math.ceil(filteredPrograms.length / ITEMS_PER_PAGE);
    const startIndex = (currentPage - 1) * ITEMS_PER_PAGE;
    const currentPrograms = filteredPrograms.slice(
        startIndex,
        startIndex + ITEMS_PER_PAGE
    );

    const handleSearch = (e) => {
        setSearchQuery(e.target.value.toLowerCase());
        setCurrentPage(1);
    };

    const getHighlightedText = (text, query) => {
        if (!query) return text;
        const parts = text.split(new RegExp(`(${query})`, "gi"));
        return parts.map((part, index) =>
            part.toLowerCase() === query.toLowerCase() ? (
                <mark key={index}>{part}</mark>
            ) : (
                part
            )
        );
    };

    const renderPrograms = () => {
        return currentPrograms.map((program) => (
            <InstanceCard
                key={program.id}
                title={getHighlightedText(program.program_name, searchQuery)}
                align="center"
                link={`/program_instance/${program.id}`}
                width="400px"
                type="program"
                data={program}
                searchQuery={searchQuery}
            />
        ));
    };

    const maxButtonsToShow = 10;
    const [buttonStartIndex, setButtonStartIndex] = React.useState(0);

    const getButtonRange = () => {
        const start = buttonStartIndex;
        const end = Math.min(buttonStartIndex + maxButtonsToShow, totalPages);
        return { start, end };
    };

    const onNextButtonSet = () => {
        if (buttonStartIndex + maxButtonsToShow < totalPages) {
            setButtonStartIndex(buttonStartIndex + maxButtonsToShow);
        }
    };

    const onPrevButtonSet = () => {
        if (buttonStartIndex > 0) {
            setButtonStartIndex(buttonStartIndex - maxButtonsToShow);
        }
    };

    const { start, end } = getButtonRange();

    return (
        <VStack bg={"black"} spacing={5} pb={5}>
            <Box
                width="100%"
                textAlign="center"
                mt={5}
                position="relative"
                px={4}
            >
                <Heading
                    as="h1"
                    size="2xl"
                    fontWeight="bold"
                    letterSpacing="wider"
                    textTransform="uppercase"
                    mb={4}
                    color="white"
                >
                    Programs
                </Heading>

                <Center>
                    <Text color="white" fontWeight="bold">
                        Number of Benefits Programs: {filteredPrograms.length}
                    </Text>
                </Center>
            </Box>
            <HStack>
                <Input
                    placeholder="Search programs..."
                    onChange={handleSearch}
                    mb="4"
                    name = "program_search"
                    data-testid="search-input"
                    bg="white"
                />
                <Select
                    placeholder="Sort By"
                    onChange={(e) => setSortField(e.target.value)}
                    mb="4"
                    data-testid="sort-select"
                    width={"200px"}
                    bg={"white"}
                >
                    <option value="programName">Program Name</option>
                    <option value="borough">Borough</option>
                    <option value="zipcode">Zip Code</option>
                    <option value="nta">Neighborhood Tabulation Area</option>
                    <option value="censusTract">Census Tract Number</option>
                </Select>
                <Select
                    placeholder="Order"
                    onChange={(e) => setSortOrder(e.target.value)}
                    mb="4"
                    data-testid="order-select"
                    width={"200px"}
                    bg={"white"}
                >
                    <option value="True">Ascending</option>
                    <option value="False">Descending</option>
                </Select>
                <Select
                    placeholder="Borough"
                    onChange={(e) => setBorough(e.target.value)}
                    mb="4"
                    data-testid="order-select"
                    width={"400px"}
                    bg={"white"}
                >
                    <option value="Bronx">Bronx</option>
                    <option value="Brooklyn">Brooklyn</option>
                    <option value="Manhattan">Manhattan</option>
                    <option value="Queens">Queens</option>
                    <option value="Staten Island">Staten Island</option>
                    <option value="STATEN IS">STATEN IS</option>
                </Select>

                <Select
                    placeholder="Program Type"
                    onChange={(e) => setProgramType(e.target.value)}
                    mb="4"
                    data-testid="order-select"
                    width={"400px"}
                    bg={"white"}
                >
                    <option value="Benefits Access Center">Benefits Access Center</option>
                    <option value="SNAP Center">SNAP Center</option>
                    <option value="Homebase (Homeless Prevention Network) Office">Homebase (Homeless Prevention Network) Office</option>
                    <option value="Homeless Drop-in Centers">Homeless Drop-in Centers</option>
                    <option value="Runaway & Homeless Youth">Runaway & Homeless Youth</option>
                </Select>
            </HStack>
            <Spacer height="20px" />
            <CardGrid data-testid="card-grid">{renderPrograms()}</CardGrid>
            <Box display="flex" justifyContent="center" mt={5}>
                {start > 0 && (
                    <Button onClick={onPrevButtonSet}>Previous</Button>
                )}
                {Array.from({ length: totalPages })
                    .slice(start, end)
                    .map((_, index) => (
                        <Button
                            key={index + start}
                            p={2}
                            mx={1}
                            mt={1}
                            cursor="pointer"
                            borderWidth={2}
                            borderRadius="md"
                            bg={
                                currentPage === index + start + 1
                                    ? "black"
                                    : "white"
                            }
                            color={
                                currentPage === index + start + 1
                                    ? "white"
                                    : "black"
                            }
                            onClick={() => setCurrentPage(index + start + 1)}
                            _hover={{
                                color: "white",
                                bg: "black",
                                transform: "scale(1.05)",
                                transition: "all 0.2s",
                            }}
                        >
                            {index + start + 1}
                        </Button>
                    ))}
                {end < totalPages && (
                    <Button onClick={onNextButtonSet}>Next</Button>
                )}
            </Box>
        </VStack>
    );
}

export default Programs;
