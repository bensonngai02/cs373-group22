import React from "react";
import { useState, useEffect } from "react";
import {
    Heading,
    Box,
    VStack,
    HStack,
    Text,
    Center,
    // Image,
} from "@chakra-ui/react";
import { useParams } from "react-router-dom";
import { InstanceCard } from "../Components/InstanceCard";
import Map from "../Components/Map";
import StreetView from "../Components/StreetView"
import axios from "axios";

function HousingInstance() {
    let params = useParams();
    let idx = params.idx;

    const [housing, setHousing] = useState({});
    const [relatedProgram1, setRelatedProgram1] = useState({});
    const [relatedProgram2, setRelatedProgram2] = useState({});
    const [relatedProgram3, setRelatedProgram3] = useState({});
    const [relatedClinic1, setRelatedClinic1] = useState({});
    const [relatedClinic2, setRelatedClinic2] = useState({});
    const [relatedClinic3, setRelatedClinic3] = useState({});

    useEffect(() => {
        // Fetch housing details
        axios
            .get(`https://new-york-aid.me/api/getHousing/${idx}`)
            .then((response) => {
                console.log("Housing Data:", response.data);
                setHousing(response.data);

                // Using Promise.all for simultaneous fetching
                const programPromises = [
                    axios.get(
                        `https://new-york-aid.me/api/getProgram/${response.data.program_inst1}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getProgram/${response.data.program_inst2}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getProgram/${response.data.program_inst3}`
                    ),
                ];

                const clinicPromises = [
                    axios.get(
                        `https://new-york-aid.me/api/getClinic/${response.data.clinic_inst1}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getClinic/${response.data.clinic_inst2}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getClinic/${response.data.clinic_inst3}`
                    ),
                ];

                Promise.all(programPromises).then((programResponses) => {
                    console.log("Program 1 Data:", programResponses[0].data);
                    console.log("Program 2 Data:", programResponses[1].data);
                    console.log("Program 3 Data:", programResponses[2].data);
                    setRelatedProgram1(programResponses[0].data);
                    setRelatedProgram2(programResponses[1].data);
                    setRelatedProgram3(programResponses[2].data);
                });

                Promise.all(clinicPromises).then((clinicResponses) => {
                    console.log("Clinic 1 Data:", clinicResponses[0].data);
                    console.log("Clinic 2 Data:", clinicResponses[1].data);
                    console.log("Clinic 3 Data:", clinicResponses[2].data);
                    setRelatedClinic1(clinicResponses[0].data);
                    setRelatedClinic2(clinicResponses[1].data);
                    setRelatedClinic3(clinicResponses[2].data);
                });
            });
    }, [idx]);

    
    return (
        <VStack mt={10}>
            <VStack>
                <Center>
                    <Map
                        width="300"
                        height="300"
                        lat={housing.latitude}
                        long={housing.longitude}
                    />
                    <div style={{padding:"12px"}}></div>
                    <StreetView
                        width="800"
                        height="300"
                        lat={housing.latitude}
                        long={housing.longitude}
                    />
                    {/* <Image
                        src={`data:image/jpeg;base64,${housing.image}`}
                        alt="Housing not displaying properly"
                        width="100%"
                        height="100%"
                        pl={5}
                        fallbackSrc="https://via.placeholder.com/300"
                    /> */}
                </Center>
                <Heading size="lg" mb={4}>
                    {housing.project_name}
                </Heading>
                <Box alignSelf={"flex-start"} alignItems={"flex-start"}>
                    <InfoText
                        label="Total Available Units:"
                        value={housing.total_units}
                    />
                    <InfoText
                        label="Number of Available Beds:"
                        value={housing.total_beds}
                    />
                    <InfoText
                        label="Completed"
                        value={`${String(housing.is_completed).toUpperCase()}`}
                    />
                    <InfoText
                        label="Typical Rent:"
                        value={housing.primary_income_class}
                    />
                    <InfoText label="Address:" value={housing.full_address} />
                    <InfoText label="Latitude" value={housing.latitude} />
                    <InfoText label="Longitude" value={housing.longitude} />
                    <InfoText label="Zip Code" value={housing.zip_code} />
                </Box>
            </VStack>

            <Heading size="xl" mb={2} alignSelf={"flex-start"} pl={5}>
                Clinics Nearby:
            </Heading>
            <HStack alignSelf={"flex-start"} pl={5} pb={5}>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedClinic1.project_name}
                        align="center"
                        link={"/clinic_instance/" + housing.clinic_inst1}
                        width="400px"
                        type="clinic"
                        data={relatedClinic1}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedClinic2.project_name}
                        align="center"
                        link={"/clinic_instance/" + housing.clinic_inst2}
                        width="400px"
                        type="clinic"
                        data={relatedClinic2}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedClinic3.project_name}
                        align="center"
                        link={"/housing_instance/" + housing.clinic_inst3}
                        width="400px"
                        type="clinic"
                        data={relatedClinic3}
                    />
                </Box>
            </HStack>
            <Heading size="xl" mb={2} alignSelf={"flex-start"} pt={4} pl={5}>
                Programs Offered:
            </Heading>
            <HStack alignSelf={"flex-start"} pl={5} pb={5}>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedProgram1.program_name}
                        align="center"
                        link={"/program_instance/" + housing.program_inst1}
                        width="400px"
                        type="program"
                        data={relatedProgram1}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedProgram2.program_name}
                        align="center"
                        link={"/program_instance/" + housing.program_inst2}
                        width="400px"
                        type="program"
                        data={relatedProgram2}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedProgram3.program_name}
                        align="center"
                        link={"/program_instance/" + housing.program_inst3}
                        width="400px"
                        type="program"
                        data={relatedProgram3}
                    />
                </Box>
            </HStack>
        </VStack>
    );
}

// Helper component to display info text
const InfoText = ({ label, value }) => (
    <VStack align="start" spacing={1} mb={2}>
        <Text as="span" fontWeight="bold">
            {label}
        </Text>
        <Text>{value}</Text>
    </VStack>
);

export default HousingInstance;
