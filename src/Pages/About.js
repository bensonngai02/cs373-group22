import React, { useState, useEffect } from "react";
import {
    Heading,
    Box,
    Grid,
    GridItem,
    Text,
    Flex,
    Accordion,
    Link,
} from "@chakra-ui/react";
import AccordionEntry from "../Components/AccordionEntry";
import ProfileCard from "../Components/ProfileCard";

// profile images
import bensonImage from "../pictures/Benson.jpg";
import osaruImage from "../pictures/Osaru.jpg";
import kapilImage from "../pictures/Kapil.jpg";
import sashankImage from "../pictures/Sashank.jpeg";
import angeloImage from "../pictures/angelo.jpeg";

// documentation images
import reactImage from "../pictures/React.png";
import chakraImage from "../pictures/Chakra.png";
import flaskImage from "../pictures/Flask.png";
import mysqlImage from "../pictures/mySQL.png";
import googleAPIImage from "../pictures/googleAPI.jpeg";
import sqlalchemyImage from "../pictures/sqla_logo.png";
import gitlabImage from "../pictures/gitlab.png"
import postmanImage from "../pictures/postman.png"

import axios from "axios";
import { InstanceCard } from "../Components/InstanceCard";

// const text1 =
// Number of unit tests will have to be manually updated
const numUnitTests = {
    bensonngai02: 6,
    oelaiho: 11,
    kapilrampalli: 0,
    aculotta: 0,
    sashankmeka7: 16,
};

function getAllCommits(contributors) {
    let totalCommits = 0;
    for (const contributor in contributors) {
        totalCommits += contributors[contributor].commits;
    }
    return totalCommits;
}

function getAllIssues(contributors) {
    let totalIssues = 0;
    for (const contributor in contributors) {
        totalIssues += contributors[contributor].issues;
    }
    return totalIssues;
}

function getStats(contributors, userName, name, img, bio, responsibilities) {
    return contributors[userName] ? (
        <ProfileCard
            name={name}
            id={contributors.name}
            image={img}
            email={contributors[userName].email}
            commits={contributors[userName].commits}
            issues={contributors[userName].issues}
            tests={contributors[userName].tests}
            bio={bio}
            responsibilities={responsibilities}
        />
    ) : (
        // Render a loading state or placeholder here
        <ProfileCard name="Loading..." />
    );
}

function About(props) {
    const [contributors, setContributors] = useState({});
    const [totalStats, setTotalStats] = useState({
        commits: 0,
        issues: 0,
        tests: 33,
    });
    const gitlabStatsURL =
        "https://gitlab.com/api/v4/projects/50576844/repository/contributors";
    const gitlabIssuesURL =
        "https://gitlab.com/api/v4/projects/50576844/issues?state=all&per_page=1000";

    useEffect(() => {
        const gitlabAccessToken = process.env.REACT_APP_GITLAB_ACCESS_TOKEN;
        const request1 = axios.get(gitlabStatsURL, {
            headers: {
                Authorization: `Bearer ${gitlabAccessToken}`,
            },
        });
        const request2 = axios.get(gitlabIssuesURL, {
            headers: {
                Authorization: `Bearer ${gitlabAccessToken}`,
            },
        });

        const nameToUsername = {
            bensonngai02: "bensonngai02",
            OsaruElaiho: "oelaiho",
            "Angelo Culotta": "aculotta",
            "Kapil Rampalli": "kapilrampalli",
            "Sashank Meka": "sashankmeka7",
        };

        Promise.all([request1, request2])
            .then((responses) => {
                const commitResponse = responses[0];
                const issueResponse = responses[1];

                // console.log(commitResponse.data);
                // console.log(issueResponse.data);
                // console.log(issueResponse.data.length);

                /*  result = accumulator object (starts as {})
                item = current element */
                const transformedContributors = commitResponse.data.reduce(
                    (result, item) => {
                        result[nameToUsername[item.name]] = {
                            email: item.email,
                            commits: item.commits,
                            issues: 0,
                            tests: numUnitTests[nameToUsername[item.name]],
                        };
                        return result;
                    },
                    {}
                );

                /* Iterating through all issues to track assignee issue counts */
                issueResponse.data.forEach((issue) => {
                    if (issue.assignees.length > 0) {
                        issue.assignees.forEach((assignee) => {
                            const userName = assignee.username;
                            if (userName in transformedContributors) {
                                transformedContributors[userName].issues += 1;
                            }
                        });
                    }
                });

                setContributors(transformedContributors);
                setTotalStats((prevStats) => ({
                    ...prevStats,
                    commits: getAllCommits(transformedContributors),
                    issues: getAllIssues(transformedContributors),
                }));
            })
            .catch((error) => {
                console.error(
                    "Failed to get gitlab repo contributor stats",
                    error
                );
            });
    }, [contributors]);

    const bensonBio =
        "Hey! I'm a junior studying CS + Math honors. I enjoy full-stack development, ML, computer vision, and computational biology. Outside of school, I play tennis for the club team.";
    const osaruBio =
        "Howdy! I'm a senior studing computer science and business. I have a deep passion for frontend development and UI/UX design. I also enjoy playing basketball and going to the gym!";
    const kapilBio =
        "Hello! I'm a senior studying Computer Science, and I'm interesting in quantitative finance and systems programming.";
    const angeloBio =
        "Howdy! I'm a senior at UT studying CS interested in software development, ML/AI, computer vision, and full stack development. My favorite pastime is hiking along Waller Creek and making my own special place there! ";
    const sashankBio =
        "Hello, I'm a senior studying CS. I'm interested in full-stack development, NLP, ML, and infrastructure. I enjoy playing basketball.";

    return (
        <Box w="100%" paddingX={"10%"}>
            <Heading size="xl" paddingY={8}>
                About
            </Heading>
            <Text>
                In recent years, New York City has experienced the highest level
                of homelessness in the United States, with approximately 85,000
                homeless people in June 2023. Our project, New York Aid, is an
                online platform committed to making a difference in the lives of
                NYC’s homeless population. It will provide comprehensive
                information, such as healthcare clinics, low-cost housing, and
                benefits programs, to serve as a beacon of hope and support for
                those in need. Ultimately, New York Aid is dedicated to
                fostering positive change and extending a helping hand to those
                who need it most in the Big Apple.{" "}
            </Text>

            <Heading size="xl" paddingTop={8}>
                Team
            </Heading>
            <Flex direction="row" display={"flex"} paddingTop={4}>
                <Text paddingEnd={12}>
                    <span style={{ fontWeight: "bold" }}>Total Commits:</span>{" "}
                    {totalStats.commits}
                </Text>
                <Text paddingEnd={12}>
                    <span style={{ fontWeight: "bold" }}>Total Issues:</span>{" "}
                    {totalStats.issues}
                </Text>
                <Text paddingEnd={12}>
                    <span style={{ fontWeight: "bold" }}>
                        Total Unit Tests:
                    </span>{" "}
                    {totalStats.tests}
                </Text>
            </Flex>
            {getStats(
                contributors,
                "aculotta",
                "Angelo Culotta",
                angeloImage,
                angeloBio,
                "Frontend, Backend"
            )}
            {getStats(
                contributors,
                "oelaiho",
                "Osaru Elaiho",
                osaruImage,
                osaruBio,
                "Frontend"
            )}
            {getStats(
                contributors,
                "sashankmeka7",
                "Sashank Meka",
                sashankImage,
                sashankBio,
                ""
            )}
            {getStats(
                contributors,
                "bensonngai02",
                "Benson Ngai",
                bensonImage,
                bensonBio,
                "Backend, Data, Deployment"
            )}
            {getStats(
                contributors,
                "kapilrampalli",
                "Kapil Rampalli",
                kapilImage,
                kapilBio,
                "Frontend"
            )}

            <Heading size="xl" paddingY={8}>
                Data Sources
            </Heading>
            <Text paddingBottom={2}>
                These data sources were all scraped programmatically using a
                REST API, which returned responses as JSON files, each
                "instance" containing a list of keys (attributes) and values
                (the value for that attribute). Some examples of attributes
                involve facility_id, city, county, population_served, etc. With
                each JSON payload we formatted values to serve as proper display
                variables, and performed analysis tasks on the data with pandas
                and numpy to create new fields that were useful to us, such as
                the largest unit available in an building, stripping out HTML
                tags from strings, or calculating the typical income requirement
                for renting at a building. After our data processing, the
                dataframe was written to a JSON file for future use.
            </Text>
            <Text paddingBottom={8}>
                It was interesting to integrate disparate data between our
                difference sources per model. As of current, we've only scraped
                1 API per model, and there are some common attributes between
                information received for each model. For example, all instances
                of every model contain some information about that instance's
                location. There were more similarities in attributes between
                healthcare clinics and housing, and less with benefits programs.
                It will be even more interesting to handle disparate data
                between API sources for a single model, which we will try to do
                in the next phase to gather sufficient data that can allow our
                instances to have more insightful relationships with one
                another.
            </Text>
            <Accordion>
                <AccordionEntry
                    title="New York Health Facility General Information"
                    text="Dataset providing information on the locations, contact information, and missions of NY medical establishments. 6142 records, accessible at https://dev.socrata.com/foundry/health.data.ny.gov/vn5v-hh5r"
                />
                <AccordionEntry
                    title="NYC Benefits Platform: Benefits and Programs Dataset"
                    text="Dataset providing descriptions and contact information for the services available to NY residents. Includes information about the location and communities served. 102 records, accessible at https://dev.socrata.com/foundry/data.cityofnewyork.us/kvhd-5fmu"
                />
                <AccordionEntry
                    title="Department of Housing: Affordable Housing Production by Building"
                    text="Dataset of the location, target income level, restrictions, and number of beds for affordable housing projects in New York City. 6816 records, accessible at https://dev.socrata.com/foundry/data.cityofnewyork.us/hg8x-zxpr"
                />
                <AccordionEntry
                    title="Directory of Benefits Access Centers"
                    text="Dataset of facility name, location, contact information, description/comments, census tract number, neighborhood tabulation area, building identificatio number, borough block lot, and benefit program/center type, accessible at https://data.cityofnewyork.us/Business/Directory-of-Benefits-Access-Centers/9d9t-bmk7"
                />
                <AccordionEntry
                    title="Directory of Homebase (Homeless Prevention Network) Offices"
                    text="Dataset of facility name, location, contact information, description/comments, census tract number, neighborhood tabulation area, building identificatio number, borough block lot, and benefit program/center type, accessible at https://data.cityofnewyork.us/Social-Services/Directory-Of-Homebase-Locations/ntcm-2w4k"
                />
                <AccordionEntry
                    title="Directory Of Homeless Drop-In Centers"
                    text="List of centers where homeless people are provided with hot meals, showers, medical help and a place to sleep. Dataset of facility name, location, contact information, description/comments, census tract number, neighborhood tabulation area, building identificatio number, borough block lot, and benefit program/center type, accessible at https://data.cityofnewyork.us/Social-Services/Directory-Of-Homeless-Drop-In-Centers/bmxf-3rd4"
                />
                <AccordionEntry
                    title="Directory of SNAP Centers"
                    text="List of SNAP Centers that offer temporary financial assistance, food stamps and Medicaid to eligible individuals. Dataset of facility name, location, contact information, description/comments, census tract number, neighborhood tabulation area, building identificatio number, borough block lot, and benefit program/center type, accessible at https://data.cityofnewyork.us/Social-Services/Directory-of-SNAP-Centers/tc6u-8rnp"
                />
                <AccordionEntry
                    title="DYCD Runaway And Homeless Youth Programs"
                    text="Facilities in New York City, by agency and site, that offer after-school programs for runaway and homeless youth, including street outreach programs, drop-in centers, crisis shelters, and transitional independent living (TIL) programs. Dataset of facility name, location, contact information, description/comments, census tract number, neighborhood tabulation area, building identificatio number, borough block lot, and benefit program/center type, accessible at https://data.cityofnewyork.us/Social-Services/DYCD-after-school-programs-Runaway-And-Homeless-Yo/ujsc-un6m"
                />
            </Accordion>

            <Heading size="xl" paddingTop={8}>
                Tech Stack & Toolchain
            </Heading>
            <Flex direction={"row"}>
                <Box
                    backgroundColor={"#e06c75"}
                    padding={6}
                    borderRadius={15}
                    marginY={6}
                    marginX={2}
                    maxWidth={"50%"}
                >
                    <Heading size="lg">Frontend</Heading>
                    <Grid templateColumns="repeat(2,1fr)" gap={4} paddingY={6}>
                        <GridItem>
                            <Link href="https://react.dev/">
                                <InstanceCard
                                    title="React.js"
                                    subtitle="Framework"
                                    image={reactImage}
                                    type="text"
                                >
                                    <Text noOfLines={[4]}>
                                        An open-source, front-end Javascript
                                        framework for building interactive user
                                        interfaces (UI) and web applications.
                                    </Text>
                                </InstanceCard>
                            </Link>
                        </GridItem>

                        <GridItem>
                            <Link href="https://chakra-ui.com/">
                                <InstanceCard
                                    title="Chakra"
                                    subtitle="Library"
                                    image={chakraImage}
                                    type="text"
                                >
                                    <Text noOfLines={[4]}>
                                        An open-source library containing a
                                        series of polished React components,
                                        similar to Bootstrap. Built on top of
                                        React UI Primitive for endless
                                        composability.
                                    </Text>
                                </InstanceCard>
                            </Link>
                        </GridItem>
                    </Grid>
                </Box>
                <Box
                    backgroundColor={"#98c379"}
                    padding={6}
                    borderRadius={15}
                    marginY={6}
                    marginX={2}
                    maxWidth={"50%"}
                >
                    <Heading size="lg">Backend</Heading>
                    <Grid
                        templateColumns="repeat(2,1fr)"
                        gap={4}
                        verticalAlign={"center"}
                        paddingY={6}
                    >
                        <GridItem>
                            <Link href="https://flask.palletsprojects.com/">
                                <InstanceCard
                                    title="Flask"
                                    subtitle="Framework"
                                    image={flaskImage}
                                    type="text"
                                >
                                    <Text noOfLines={4}>
                                        A lightweight and versatile micro web
                                        framework written in Python. Ideal for
                                        creating robust web applications with
                                        ease.
                                    </Text>
                                </InstanceCard>
                            </Link>
                        </GridItem>

                        <GridItem>
                            <Link href="https://www.mysql.com/">
                                <InstanceCard
                                    title="MySQL"
                                    subtitle="Database"
                                    image={mysqlImage}
                                    type="text"
                                >
                                    <Text noOfLines={4}>
                                        A reliable and widely-used relational
                                        database system. It's an essential tool
                                        for data storage with high performance.
                                    </Text>
                                </InstanceCard>
                            </Link>
                        </GridItem>

                        <GridItem>
                            <Link href="https://www.sqlalchemy.org/">
                                <InstanceCard
                                    title="SQLAlchemy"
                                    subtitle="ORM"
                                    image={sqlalchemyImage}
                                    type="text"
                                >
                                    <Text noOfLines={4}>
                                        An Object Relational Mapper (ORM) that
                                        provides a set of high-level API to
                                        connect Python applications to
                                        relational databases.
                                    </Text>
                                </InstanceCard>
                            </Link>
                        </GridItem>

                        <GridItem>
                            <Link href="https://cloud.google.com/apis">
                                <InstanceCard
                                    title="Google API"
                                    subtitle="API Suite"
                                    image={googleAPIImage}
                                    type="text"
                                >
                                    <Text noOfLines={4}>
                                        A collection of robust tools and
                                        services offered by Google, enabling
                                        integration with Google services and
                                        enhancing application capabilities.
                                    </Text>
                                </InstanceCard>
                            </Link>
                        </GridItem>
                    </Grid>
                </Box>
            </Flex>


            <Box paddingBottom={12}>
                <Heading size="lg" paddingTop={8}>
                    Project Resources
                </Heading>
                <Box
                    backgroundColor={"#c678dd"}
                    padding={6}
                    borderRadius={15}
                    marginY={6}
                    marginX={2}
                >
                    <Grid templateColumns="repeat(2,1fr)" gap={4} paddingY={6}>
                        <GridItem>
                            <Link href="https://gitlab.com/bensonngai02/cs373-group22">
                                <InstanceCard
                                    title="Gitlab"
                                    subtitle="Code Repository"
                                    image={gitlabImage}
                                    type="text"
                                >
                                    <Text noOfLines={4}>
                                        Our code repository for this website's entire full-stack implementation.
                                    </Text>
                                </InstanceCard>
                            </Link>
                        </GridItem>
                        <GridItem>
                            <Link href="https://documenter.getpostman.com/view/25932782/2s9YJXYjvs#a662fbe2-ae89-4d41-b4fe-c6286cbc21c4">
                                <InstanceCard
                                    title="Postman"
                                    subtitle="API Documentation"
                                    image={postmanImage}
                                    type="text"
                                >
                                    <Text noOfLines={4}>
                                        Our New York Aid API documentation, involving API calls to obtain, filter, and search information about any of our models. 
                                    </Text>
                                </InstanceCard>
                            </Link>
                        </GridItem>
                    </Grid>
                </Box>
            </Box>
        </Box>
    );
}

export default About;
