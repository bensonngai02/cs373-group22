import React from "react";
import { useState, useEffect } from "react";
import {
    Heading,
    Box,
    VStack,
    HStack,
    Text,
    Center,
    // Image,
} from "@chakra-ui/react";
import { useParams } from "react-router-dom";
import { InstanceCard, DynamicCardBody } from "../Components/InstanceCard";
import Map from "../Components/Map";
import StreetView from "../Components/StreetView"
import axios from "axios";

function ProgramInstance() {
    let params = useParams();
    let idx = params.idx;

    const [program, setProgram] = useState({});
    const [relatedClinic1, setRelatedClinic1] = useState({});
    const [relatedClinic2, setRelatedClinic2] = useState({});
    const [relatedClinic3, setRelatedClinic3] = useState({});
    const [relatedHousing1, setRelatedHousing1] = useState({});
    const [relatedHousing2, setRelatedHousing2] = useState({});
    const [relatedHousing3, setRelatedHousing3] = useState({});

    useEffect(() => {
        // Fetch program details
        axios
            .get(`https://new-york-aid.me/api/getProgram/${idx}`)
            .then((response) => {
                console.log("Program Data:", response.data);
                setProgram(response.data);

                // Using Promise.all for simultaneous fetching
                const clinicPromises = [
                    axios.get(
                        `https://new-york-aid.me/api/getClinic/${response.data.clinic_inst1}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getClinic/${response.data.clinic_inst2}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getClinic/${response.data.clinic_inst3}`
                    ),
                ];

                const housingPromises = [
                    axios.get(
                        `https://new-york-aid.me/api/getHousing/${response.data.housing_inst1}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getHousing/${response.data.housing_inst2}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getHousing/${response.data.housing_inst3}`
                    ),
                ];

                Promise.all(clinicPromises).then((clinicResponses) => {
                    console.log("Clinic 1 Data:", clinicResponses[0].data);
                    console.log("Clinic 2 Data:", clinicResponses[1].data);
                    console.log("Clinic 3 Data:", clinicResponses[2].data);
                    setRelatedClinic1(clinicResponses[0].data);
                    setRelatedClinic2(clinicResponses[1].data);
                    setRelatedClinic3(clinicResponses[2].data);
                });

                Promise.all(housingPromises).then((housingResponses) => {
                    console.log("Housing 1 Data:", housingResponses[0].data);
                    console.log("Housing 2 Data:", housingResponses[1].data);
                    console.log("Housing 3 Data:", housingResponses[2].data);
                    setRelatedHousing1(housingResponses[0].data);
                    setRelatedHousing2(housingResponses[1].data);
                    setRelatedHousing3(housingResponses[2].data);
                });
            });
    }, [idx]);

    console.log("facil:", program.clinic_inst1);

    return (
        <VStack mt={10}>
            <VStack>
                <Center>
                    <Map
                        width="300"
                        height="300"
                        lat={program.latitude}
                        long={program.longitude}
                    />
                    <div style={{padding:"12px"}}></div>
                    <StreetView
                        width="800"
                        height="300"
                        lat={program.latitude}
                        long={program.longitude}
                    />
                    {/* <Image
                        src={`data:image/jpeg;base64,${program.image}`}
                        alt="Program not displaying properly"
                        width="100%"
                        height="100%"
                        pl={5}
                        fallbackSrc="https://via.placeholder.com/300"
                    /> */}
                </Center>
                <Heading size="lg" mb={4}>
                    {program.program_name}
                </Heading>
                <Box alignSelf={"flex-start"} alignItems={"flex-start"}>
                    <InfoText label="Address:" value={program.address} />
                    <InfoText label="Borough:" value={program.borough} />
                    <InfoText
                        label="Census Tract:"
                        value={program.census_tract}
                    />
                    <InfoText label="Latitude" value={program.latitude} />
                    <InfoText label="Longitude" value={program.longitude} />
                    <InfoText
                        label="Neighborhood Tabulation Area"
                        value={program.nta}
                    />
                    <InfoText label="Phone" value={program.phone} />
                    <InfoText label="Type" value={program.type} />
                    <InfoText label="Zip Code" value={program.zip_code} />
                </Box>
            </VStack>

            <Heading size="xl" mb={2} alignSelf={"flex-start"} pl={5}>
                Clinics Nearby:
            </Heading>
            <HStack alignSelf={"flex-start"} pl={5} pb={5}>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedClinic1.facility_name}
                        align="center"
                        link={"/clinic_instance/" + program.clinic_inst1}
                        width="400px"
                        type="clinic"
                        data={relatedClinic1}
                    >
                        <DynamicCardBody data={relatedClinic1} />
                    </InstanceCard>
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedClinic2.facility_name}
                        align="center"
                        link={"/clinic_instance/" + program.clinic_inst2}
                        width="400px"
                        type="clinic"
                        data={relatedClinic2}
                    >
                        <DynamicCardBody data={relatedClinic2} />
                    </InstanceCard>
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedClinic3.facility_name}
                        align="center"
                        link={"/clinic_instance/" + program.clinic_inst3}
                        width="400px"
                        type="clinic"
                        data={relatedClinic3}
                    >
                        <DynamicCardBody data={relatedClinic3} />
                    </InstanceCard>
                </Box>
            </HStack>
            <Heading size="xl" mb={2} alignSelf={"flex-start"} pt={4} pl={5}>
                Affordable Housing Nearby:
            </Heading>
            <HStack alignSelf={"flex-start"} pl={5} pb={5}>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedHousing1.project_name}
                        align="center"
                        link={"/housing_instance/" + program.housing_inst1}
                        width="400px"
                        type="housing"
                        data={relatedHousing1}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedHousing2.project_name}
                        align="center"
                        link={"/housing_instance/" + program.housing_inst2}
                        width="400px"
                        type="housing"
                        data={relatedHousing2}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedHousing3.project_name}
                        align="center"
                        link={"/housing_instance/" + program.housing_inst3}
                        width="400px"
                        type="housing"
                        data={relatedHousing3}
                    />
                </Box>
            </HStack>
        </VStack>
    );
}

// Helper component to display info text
const InfoText = ({ label, value }) => (
    <VStack align="start" spacing={1} mb={2}>
        <Text as="span" fontWeight="bold">
            {label}
        </Text>
        <Text>{value}</Text>
    </VStack>
);

export default ProgramInstance;
