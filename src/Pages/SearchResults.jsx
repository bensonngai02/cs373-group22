import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import {Center,  Tabs, TabList, TabPanels, Tab, TabPanel } from '@chakra-ui/react'
import { useParams } from "react-router-dom";
import { InstanceCard } from "../Components/InstanceCard";
import CardGrid from "../Components/CardGrid";


function SearchResults() {
    let params = useParams();
    const search = encodeURIComponent(params.search);

    const [clinics, setClinics] = useState([]);
    const [housing, setHousing] = useState([]);
    const [programs, setPrograms] = useState([]);

    const getData = (url, setHook) => {
        axios
            .get(url)
            .then((response) => {
                setHook(response.data);
                //console.log(response.data);
            })
            .catch((error) => {
                if (error.response) {
                    // The request was made and the server responded with a status code that falls out of the range of 2xx
                    /*console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);*/
                } else if (error.request) {
                    // The request was made but no response was received
                    console.error(
                        "Error fetching the data: No response from the server."
                    );
                } else {
                    // Something happened in setting up the request that triggered an Error
                    console.error(
                        "Error in setting up the request:",
                        error.message
                    );
                }
            });
    };

    useEffect(() => {
        getData("https://new-york-aid.me/api/searchClinic/" + search, setClinics);
        getData("https://new-york-aid.me/api/searchHousing/" + search, setHousing);
        getData("https://new-york-aid.me/api/searchProgram/" + search, setPrograms);
    }, [search]);

    return (
        <Tabs>
            <Center width="100%">
            <TabList >
                <Tab>Clinics</Tab>
                <Tab>Housing</Tab>
                <Tab>Programs</Tab>
            </TabList>
            </Center>
            <TabPanels>
                <TabPanel>
                    <CardGrid data-testid="search-card-grid">
                        {clinics.map((clinic) => (
                            <InstanceCard
                                title={getHighlightedText(clinic.facility_name, params.search)}
                                align="center"
                                link={`/clinic_instance/${clinic.id}`}
                                width="400px"
                                type="clinic"
                                data={clinic}
                                searchQuery={params.search}
                            />
                        ))}
                    </CardGrid>
                </TabPanel>
                <TabPanel>
                    <CardGrid>
                        {housing.map((house) => (
                            <InstanceCard
                                title={getHighlightedText(house.project_name, params.search)}
                                align="center"
                                link={`/housing_instance/${house.id}`}
                                width="400px"
                                type="housing"
                                data={house}
                                searchQuery={params.search}
                            />
                        ))}
                    </CardGrid>
                </TabPanel>
                <TabPanel>
                    <CardGrid>
                        {programs.map((program) => (
                            <InstanceCard
                                title={getHighlightedText(program.program_name, params.search)}
                                align="center"
                                link={`/program_instance/${program.id}`}
                                width="400px"
                                type="program"
                                data={program}
                                searchQuery={params.search}
                            />
                        ))}
                    </CardGrid>
                </TabPanel>
            </TabPanels>
            </Tabs>
    );
}

const getHighlightedText = (text, query) => {
    if (!query) return text;
    const parts = text.split(new RegExp(`(${query})`, "gi"));
    return parts.map((part, index) =>
        part.toLowerCase() === query.toLowerCase() ? (
            <mark key={index}>{part}</mark>
        ) : (
            part
        )
    );
};

export default SearchResults;