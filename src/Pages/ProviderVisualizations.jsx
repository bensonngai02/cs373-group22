import React, { useEffect, useState } from "react";
import { VStack, Box, Heading, Text } from "@chakra-ui/react";
import { PieChart, Pie, Cell, ScatterChart, Scatter, XAxis as RechartsXAxis, YAxis as RechartsYAxis, CartesianGrid } from 'recharts';
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    VerticalBarSeries,
} from 'react-vis';
import axios from "axios";

const ProviderVisualizations = () => {

    const [genderData, setGenderData] = useState([
        { gender: "Male", count: 0, fill: '#ff0000'},
        { gender: "Female", count: 0, fill: '#00ff00'}
    ]);

    const [zipCodeData, setZipCodeData] = useState([]);
    const [nursingData, setNursingData] = useState([]);

    useEffect(() => {
        axios.get('https://backend.goldenyearsguide.me/api/geriatricians?city=DALLAS')
        .then(response => {
            const data1 = response.data.data;
            const updatedGenderData = [
                { gender: "Male", count: 0},
                { gender: "Female", count: 0}
            ]; 
            
            data1.forEach((entry) => {
                if (entry.hasOwnProperty('gender')) {
                    if (entry.gender === 'M') {
                    updatedGenderData[0]["count"]++;
                    } else if (entry.gender === 'F') {
                    updatedGenderData[1]["count"]++;
                    }
                }
                setGenderData(updatedGenderData);
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }, []);

    useEffect(() => {
        axios
            .get("https://backend.goldenyearsguide.me/api/zipcodes?population=20000")
            .then((response) => {
                let bars = new Map();
                response.data.data.forEach((zipcode) => {
                    if (!bars.has(zipcode.city)) {
                        bars.set(zipcode.city, 1);
                    } else {
                        bars.set(zipcode.city, bars.get(zipcode.city) + 1);
                    }
                });
                setZipCodeData(Array.from(bars,
                    (([city, count]) => ({ "y": count, "x": city }))));
            })
    }, []);

    useEffect(() => {
        axios
            .get("https://backend.goldenyearsguide.me/api/nursing_homes?ratingmin=4.5")
            .then((response) => {
                let updatedNursingData = [];
                response.data.data.forEach((entry) => {
                    updatedNursingData.push({'lat': entry.latitude, 'long': entry.longitude})
                });
                setNursingData(updatedNursingData);
            })
    }, []);
    console.log("hello")

    // Create a hash map with counts
    return (
        <VStack spacing={8} p={8} align="stretch">
            <Box>
                <Heading mb={4}>Provider Visualizations</Heading>
            </Box>
            <Box boxShadow="md" p="6" rounded="md" bg="white">
                <Heading size="md">Zipcodes In Each City</Heading>
                <Text mt={2}>
                    Number of Zipcodes (with at least 20,000 residents) In Each City
                </Text>
                <XYPlot xType="ordinal" width={1000} height={300} xDistance={1000}
                    style={{
                        fontSize: '0.6em'
                    }}>
                    <VerticalGridLines />
                    <HorizontalGridLines />
                    <XAxis />
                    <YAxis />
                    <VerticalBarSeries data={zipCodeData} />
                </XYPlot>
            </Box>
            <Box boxShadow="md" p="6" rounded="md" bg="white">
                <Heading size="md">Nursing Home Locations (Latitude & Longitude) with At Least 4.5 Rating</Heading>
                <Text mt={2}>
                    Each green dot represents a clinic on a latitude-longitude grid rated at least 4.5
                </Text>
                <ScatterChart width={500} height={500}> 
                    <CartesianGrid /> 
                    <RechartsXAxis type="number" dataKey="lat" label={{ value: 'Latitude', position: 'insideBottom', offset: -5 }}/> 
                    <RechartsYAxis type="number" dataKey="long" label={{ value: 'Longitude', angle: -90, position: 'insideLeft' }}/> 
                    <Scatter data={nursingData} fill="green" /> 
                </ScatterChart> 
            </Box>
            <Box boxShadow="md" p="6" rounded="md" bg="white">
                <Heading size="md">Gender Split Between Geriatricians Located in Dallas</Heading>
                <Text mt={2}>
                    Percentage of male vs female geriatricians working in Dallas
                </Text>
                <PieChart width={700} height={700}>
                    <Pie 
                        data={genderData} 
                        cx="50%" cy="50%" labelLine={false} outerRadius={250} fill="red" dataKey="count" label={({ name, percent, count }) => `${name} ${(percent * 100).toFixed(0)}% (${count})`} nameKey="gender">
                        {genderData.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={index % 2 === 0 ? '#82CA9D' : '#8884D8'} />
                        ))}
                    </Pie>
                </PieChart>
            </Box>
        </VStack>
    );
};

export default ProviderVisualizations;
