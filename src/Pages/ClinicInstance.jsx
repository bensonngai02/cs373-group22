import React from "react";
import { useState, useEffect } from "react";
import {
    Heading,
    Text,
    VStack,
    HStack,
    Box,
    Center,
    // Image,
} from "@chakra-ui/react";
import { useParams } from "react-router-dom";
import { InstanceCard } from "../Components/InstanceCard";
import Map from "../Components/Map";
import StreetView from "../Components/StreetView"
import axios from "axios";

function ClinicInstance() {
    let params = useParams();
    let idx = params.idx;

    const [clinic, setClinic] = useState({});
    const [relatedProgram1, setRelatedProgram1] = useState({});
    const [relatedProgram2, setRelatedProgram2] = useState({});
    const [relatedProgram3, setRelatedProgram3] = useState({});
    const [relatedHousing1, setRelatedHousing1] = useState({});
    const [relatedHousing2, setRelatedHousing2] = useState({});
    const [relatedHousing3, setRelatedHousing3] = useState({});

    useEffect(() => {
        // Fetch clinic details
        axios
            .get(`https://new-york-aid.me/api/getClinic/${idx}`)
            .then((response) => {
                console.log("Clinic Data:", response.data);
                setClinic(response.data);

                // Using Promise.all for simultaneous fetching
                const programPromises = [
                    axios.get(
                        `https://new-york-aid.me/api/getProgram/${response.data.program_inst1}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getProgram/${response.data.program_inst2}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getProgram/${response.data.program_inst3}`
                    ),
                ];

                const housingPromises = [
                    axios.get(
                        `https://new-york-aid.me/api/getHousing/${response.data.housing_inst1}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getHousing/${response.data.housing_inst2}`
                    ),
                    axios.get(
                        `https://new-york-aid.me/api/getHousing/${response.data.housing_inst3}`
                    ),
                ];

                Promise.all(programPromises)
                    .then((programResponses) => {
                        console.log(
                            "Program 1 Data:",
                            programResponses[0].data
                        );
                        console.log(
                            "Program 2 Data:",
                            programResponses[1].data
                        );
                        console.log(
                            "Program 3 Data:",
                            programResponses[2].data
                        );
                        setRelatedProgram1(programResponses[0].data);
                        setRelatedProgram2(programResponses[1].data);
                        setRelatedProgram3(programResponses[2].data);
                    })
                    .catch((error) => {
                        console.error("Error fetching program data:", error);
                    });

                Promise.all(housingPromises)
                    .then((housingResponses) => {
                        console.log(
                            "Housing 1 Data:",
                            housingResponses[0].data
                        );
                        console.log(
                            "Housing 2 Data:",
                            housingResponses[1].data
                        );
                        console.log(
                            "Housing 3 Data:",
                            housingResponses[2].data
                        );
                        setRelatedHousing1(housingResponses[0].data);
                        setRelatedHousing2(housingResponses[1].data);
                        setRelatedHousing3(housingResponses[2].data);
                    })
                    .catch((error) => {
                        console.error("Error fetching housing data:", error);
                    });
            })
            .catch((error) => {
                console.error("Error fetching clinic data:", error);
            });
    }, [idx]);

    return (
        <VStack mt={10}>
            <VStack>
                <Center>
                    <Map
                        width="300"
                        height="300"
                        lat={clinic.latitude}
                        long={clinic.longitude}
                    />
                    <div style={{padding:"12px"}}></div>
                    <StreetView
                        width="800"
                        height="300"
                        lat={clinic.latitude}
                        long={clinic.longitude}
                    />
                    {/* <Image
                        src={`data:image/jpeg;base64,${clinic.image}`}
                        alt="Clinic not displaying properly"
                        width="100%"
                        height="100%"
                        pl={5}
                        fallbackSrc='https://via.placeholder.com/300'
                    /> */}
                </Center>
                <Heading size="lg" mb={4}>
                    {clinic?.facility_name}
                </Heading>
                <Box alignSelf={"flex-start"} alignItems={"flex-start"}>
                    <InfoText label="Address:" value={clinic.full_address} />
                    <InfoText
                        label="Phone:"
                        value={clinic.fac_phone ?? "N/A"}
                    />
                    <InfoText label="Zip Code:" value={clinic.fac_zip} />
                    <InfoText label="County:" value={clinic.county} />
                    <InfoText label="Latitude:" value={clinic.latitude} />
                    <InfoText label="Longitude:" value={clinic.longitude} />
                    <InfoText
                        label="Website:"
                        value={clinic?.web_site ?? "N/A"}
                    />
                    <InfoText
                        label="Profit/Non-profit:"
                        value={clinic?.ownership_type}
                    />
                </Box>
            </VStack>
            <Heading size="xl" mb={2} alignSelf={"flex-start"} pl={5}>
                Affordable Housing Nearby:
            </Heading>
            <HStack alignSelf={"flex-start"} pl={5} pb={5}>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedHousing1.project_name}
                        align="center"
                        link={"/housing_instance/" + clinic.housing_inst1}
                        width="400px"
                        type="housing"
                        data={relatedHousing1}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedHousing2.project_name}
                        align="center"
                        link={"/housing_instance/" + clinic.housing_inst2}
                        width="400px"
                        type="housing"
                        data={relatedHousing2}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedHousing3.project_name}
                        align="center"
                        link={"/housing_instance/" + clinic.housing_inst3}
                        width="400px"
                        type="housing"
                        data={relatedHousing3}
                    />
                </Box>
            </HStack>
            <Heading size="xl" mb={2} alignSelf={"flex-start"} pt={4} pl={5}>
                Programs Offered:
            </Heading>
            <HStack alignSelf={"flex-start"} pl={5} pb={5}>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedProgram1.program_name}
                        align="center"
                        link={"/program_instance/" + clinic.program_inst1}
                        width="400px"
                        type="program"
                        data={relatedProgram1}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedProgram2.program_name}
                        align="center"
                        link={"/program_instance/" + clinic.program_inst2}
                        width="400px"
                        type="program"
                        data={relatedProgram2}
                    />
                </Box>
                <Box pr={5}>
                    <InstanceCard
                        title={relatedProgram3.program_name}
                        align="center"
                        link={"/program_instance/" + clinic.program_inst3}
                        width="400px"
                        type="program"
                        data={relatedProgram2}
                    />
                </Box>
            </HStack>
        </VStack>
    );
}

// Helper component to display info text
const InfoText = ({ label, value }) => (
    <VStack align="start" spacing={1} mb={2}>
        <Text as="span" fontWeight="bold">
            {label}
        </Text>
        <Text>{value}</Text>
    </VStack>
);

export default ClinicInstance;
