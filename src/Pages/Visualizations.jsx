import React, { useState, useEffect } from "react";
import { VStack, Box, Heading, Text } from "@chakra-ui/react";
import {
    XYPlot,
    XAxis,
    YAxis,
    VerticalGridLines,
    HorizontalGridLines,
    HorizontalBarSeries,
    VerticalBarSeries,
    RadialChart
} from 'react-vis';
import axios from "axios";

const Visualizations = () => {
    let [viz1, setViz1] = useState([]);
    let [viz2, setViz2] = useState([]);
    let [viz3, setViz3] = useState([]);

    useEffect(() => {
        axios
            .get("https://new-york-aid.me/api/getAllPrograms")
            .then((response) => {
                let bars = new Map();
                response.data.forEach((program) => {
                    if (!bars.has(program.type)) {
                        bars.set(program.type, 1);
                    } else {
                        bars.set(program.type, bars.get(program.type) + 1);
                    }
                });
                setViz1(Array.from(bars,
                    (([type, count]) => ({ "y": count, "x": type }))));
            })
    });

    useEffect(() => {
        axios
            .get("https://new-york-aid.me/api/getAllHousing")
            .then((response) => {
                let wedges = new Map();
                response.data.forEach((program) => {
                    if (!wedges.has(program.primary_income_class)) {
                        wedges.set(program.primary_income_class, 1);
                    } else {
                        wedges.set(program.primary_income_class, wedges.get(program.primary_income_class) + 1);
                    }
                });
                setViz2(Array.from(wedges,
                    (([type, count]) => ({ "angle": count, "label": type.substring(0, type.indexOf(' ')) }))));
            })
    });

    useEffect(() => {
        axios
            .get("https://new-york-aid.me/api/getAllClinics")
            .then((response) => {
                let bars = new Map();
                response.data.forEach((program) => {
                    if (!bars.has(program.ownership_type)) {
                        bars.set(program.ownership_type, 1);
                    } else {
                        bars.set(program.ownership_type, bars.get(program.ownership_type) + 1);
                    }
                });
                setViz3(Array.from(bars,
                    (([type, count]) => ({ "x": count, "y": type }))));
            })
    });


    return (
        <VStack spacing={8} p={8} align="stretch">
            <Box>
                <Heading mb={4}>Visualizations</Heading>
            </Box>
            <Box boxShadow="md" p="6" rounded="md" bg="white">
                <Heading size="md">Types of Programs</Heading>
                <Text mt={2}>Number of each type of Program</Text>
                <XYPlot xType="ordinal" width={1000} height={300} xDistance={1000}
                    style={{
                        fontSize: '0.6em'
                    }}>
                    <VerticalGridLines />
                    <HorizontalGridLines />
                    <XAxis />
                    <YAxis />
                    <VerticalBarSeries data={viz1} />
                </XYPlot>
            </Box>
            <Box boxShadow="md" p="6" rounded="md" bg="white">
                <Heading size="md">Income Classes of Housing</Heading>
                <Text mt={2}>Percentage of Low-Cost Housing by Income Class</Text>
                <RadialChart
                    width={600}
                    height={600}
                    data={viz2}
                    radius={200}
                    showLabels={true}
                    labelsRadiusMultiplier={1.2}></RadialChart>
            </Box>
            <Box boxShadow="md" p="6" rounded="md" bg="white">
                <Heading size="md">Types of Clinics</Heading>
                <Text mt={2}>Number of Clinics for each type of ownership</Text>
                <XYPlot yType="ordinal"
                    width={800} height={500}
                    margin={{ "left": 250 }}>
                    <VerticalGridLines />
                    <HorizontalGridLines />
                    <XAxis />
                    <YAxis />
                    <HorizontalBarSeries data={viz3} />
                </XYPlot>
            </Box>
        </VStack >
    );
};

export default Visualizations;
