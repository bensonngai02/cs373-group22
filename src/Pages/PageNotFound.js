import { Link } from "@chakra-ui/react"

function PageNotFound(props){
    var referrer =  document.referrer;
    return(
        <div>
            <h1>Page Not Found</h1>
            <Link href={referrer}>Return</Link>
        </div>
    )
}

export default PageNotFound;

