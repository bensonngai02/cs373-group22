import InstanceCard from '../Components/InstanceCard';
import NavBar from '../Components/NavBar';
import CardGrid from '../Components/CardGrid'
import logo from '../logo.svg';

function App() {
  return (
    <div>
      <NavBar />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <CardGrid>
        <InstanceCard title="Title" subtitle="Subtitle" text="OMG!!!" buttonName="button"/>
      </CardGrid>
      
    </div>
    
  );
}

export default App;