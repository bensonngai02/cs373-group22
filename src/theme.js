import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
    styles: {
        global: {
            "@keyframes rippleEffect": {
                "0%": {
                    backgroundPosition: "0% 0%",
                },
                "100%": {
                    backgroundPosition: "0% 100%",
                },
            },
            ".ripple": {
                background: "linear-gradient(transparent 50%, white 50%)",
                backgroundSize: "100% 200%",
                animation: "rippleEffect 1s forwards",
            },
        },
    },
});

export default theme;
