import clinic_data_src from "./api/scraped/clinics.json"
import housing_data_src from "./api/scraped/housing.json"
import programs_data_src from "./api/scraped/programs.json"

export const clinic_data =  clinic_data_src;
export const housing_data = housing_data_src;
export const program_data = programs_data_src;
 