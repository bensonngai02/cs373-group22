// GlobalStyles.js
import { css, Global } from "@emotion/react";
const RippleKeyframes = css`
    @keyframes ripple {
        0% {
            top: -50%;
            height: 100%;
        }
        100% {
            top: 0%;
            height: 0%;
        }
    }
`;



export const GlobalStyles = () => <Global styles={RippleKeyframes} />;
