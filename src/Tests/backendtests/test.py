import unittest
import requests
api_url = "https://api.new-york-aid.me/api"
class TestingClass(unittest.TestCase):
    def test_1(self):
        #check program count returned
        response = requests.get(api_url+"/getAllPrograms")
        self.assertEqual(response.status_code, 200)

        data = response.json()


        self.assertEqual(len(data), 77)
        
    def test_2(self):
        #check clinic count returned
        response = requests.get(api_url+"/getAllClinics")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 267)

    def test_3(self):
        #check housing count returned
        response = requests.get(api_url+"/getAllHousing")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 968)

    def test_4(self):
        #check if clinic id matches
        response = requests.get(api_url+"/getClinic/2")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['id'], 2)
        self.assertEqual(data['facility_name'], 'Samaritan Health Service University')

    def test_5(self):
        #check if program id matches
        response = requests.get(api_url+"/getProgram/1")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['id'], 1)
        self.assertEqual(data['bbl'], 2028560020)
    def test_6(self):
        #check if housing id matches
        response = requests.get(api_url+"/getHousing/1")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['id'], 1)
        self.assertEqual(data['full_address'], '2319 3 AVENUE, Manhattan, NYC, 10035')
    def test_7(self):
        #check filtering on allclinics
        response = requests.get(api_url+"/getAllClinics?zipcode=10452")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 6)
    def test_8(self):
        #check filtering on allhousing
        response = requests.get(api_url+"/getAllHousing?totalUnits=161")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 3)
    def test_9(self):
        #check filtering on allhousing by beds
        response = requests.get(api_url+"/getAllHousing?totalBeds=373")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 1)
    def test_10(self):
        #test filtering on programs by borough
        response = requests.get(api_url+"/getAllPrograms?borough=Bronx")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 20)

    def test_11(self):
        #test clinic search
        response = requests.get(api_url+"/searchClinic/samaritan")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]["facility_name"], "Samaritan Health Service University") 
    
    def test_12(self):
        #test housing search
        response = requests.get(api_url+"/searchHousing/Rochester")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 9)
     
    def test_13(self):
        #test program search
        response = requests.get(api_url+"/searchProgram/family")
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 2)

if __name__ == '__main__':
    unittest.main()