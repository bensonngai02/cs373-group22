import unittest
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import time
url = 'https://www.new-york-aid.me/'
class SeleniumTests(unittest.TestCase):
    def setUp(self) -> None:
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument("--disable-gpu")
        options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(options=options)
        self.driver.implicitly_wait(10)
        self.driver.get(url)
        return super().setUp()
    
    def tearDown(self) -> None:
        self.driver.quit()
        return super().tearDown()
    #tests 1-5: test site subpages
    def test_1(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="About"]')
        button.click()
        self.assertEqual(self.driver.current_url, url+ "about")
    def test_2(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Healthcare Clinics"]')
        button.click()
        self.assertEqual(self.driver.current_url, url+ "clinics")
    def test_3(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Low-Cost Housing"]')
        button.click()
        self.assertEqual(self.driver.current_url, url+ "housing")
    def test_4(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Benefits Programs"]')
        button.click()
        self.assertEqual(self.driver.current_url, url+ "programs")

    #tests 6-9: check for important keywords/contributor names
    def test_6(self):
        self.driver.get(url)
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Illuminating Paths, Changing Lives')]")
        self.assertEqual(text.text, "Illuminating Paths, Changing Lives")
    def test_5(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="About"]')
        button.click()
        text = self.driver.find_element(By.XPATH, "//h2[contains(text(), 'Team')]")
        self.assertEqual(text.text, "Team")

    def test_7(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="About"]')
        button.click()
        members = ["Angelo Culotta", "Benson Ngai", "Osaru Elaiho", "Sashank Meka", "Kapil Rampalli"]
        for member in members:
            member_item = self.driver.find_element(By.XPATH, f"//h2[contains(text(), '{member}')]")
            self.assertEqual(member_item.text, member)
    #tests 8-10: check for dynamic counts
    def test_8(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Healthcare Clinics"]')
        button.click()
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Number of Clinics: ')]")
        self.assertEqual(text.text, "Number of Clinics: 267")
    def test_9(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Low-Cost Housing"]')
        button.click()
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Number of Low-Cost Housing Buildings:')]")
        self.assertEqual(text.text, "Number of Low-Cost Housing Buildings: 968")
    def test_10(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Benefits Programs"]')
        button.click()
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Number of Benefits Programs: ')]")
        self.assertEqual(text.text, "Number of Benefits Programs: 77")
    #tests 11-16: check cards for field data based on search results
    def test_11(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Healthcare Clinics"]')
        button.click()
        time.sleep(3)
        search_bar = self.driver.find_element(By.NAME, "search")

        search_bar.send_keys("samaritan" + Keys.ENTER)
        #check if all fields are present
        fields = ["Address", "Phone", "Zip Code", "County", "Website", "Profit/Non-profit"]
        for field in fields:
            query_str = f"//*[contains(text(), '{field}')]"
            text = self.driver.find_element(By.XPATH, query_str)
            self.assertEqual(text.text, f"{field}:")
    def test_12(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Low-Cost Housing"]')
        button.click()
        time.sleep(3)
        search_bar = self.driver.find_element(By.NAME, "search")

        search_bar.send_keys("parcel" + Keys.ENTER)
        tab = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Housing"]')
        tab.click()
        time.sleep(3)
        #check if all fields are present
        fields = ["Project Name", "Address", "Total Units", "Total Beds", "Completed", "Income Class", "Zip Code"]
        for field in fields:
            query_str = f"//*[contains(text(), '{field}')]"
            text = self.driver.find_element(By.XPATH, query_str)
            self.assertEqual(text.text, f"{field}:")
    def test_13(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Benefits Programs"]')
        button.click()
        time.sleep(3)
        search_bar = self.driver.find_element(By.NAME, "search")

        search_bar.send_keys("dyckman" + Keys.ENTER)
        tab = self.driver.find_element(By.ID ,'tabs-:r6:--tab-2')
        tab.click()
        time.sleep(3)
        #check if all fields are present
        fields = ["Address", "Borough", "Phone", "Type", "Zip Code"]
        for field in fields:
            query_str = f"//*[contains(text(), '{field}')]"
            text = self.driver.find_element(By.XPATH, query_str)
            self.assertEqual(text.text, f"{field}:")
    def test_14(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Benefits Programs"]')
        button.click()
        time.sleep(3)
        search_bar = self.driver.find_element(By.NAME, "program_search")

        search_bar.send_keys("89" + Keys.ENTER)
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Number of Benefits Programs: ')]")
        self.assertEqual(text.text, "Number of Benefits Programs: 6")
    def test_15(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Healthcare Clinics"]')
        button.click()
        time.sleep(3)
        search_bar = self.driver.find_element(By.NAME, "clinic_search")

        search_bar.send_keys("kings" + Keys.ENTER)
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Number of Clinics: ')]")
        self.assertEqual(text.text, "Number of Clinics: 101")
    def test_16(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH ,'//button[normalize-space()="Low-Cost Housing"]')
        button.click()
        time.sleep(3)
        search_bar = self.driver.find_element(By.NAME, "housing_search")

        search_bar.send_keys("roch" + Keys.ENTER)
        time.sleep(3)
        text = self.driver.find_element(By.XPATH, "//*[contains(text(), 'Number of Low-Cost Housing Buildings:')]")
        self.assertEqual(text.text, "Number of Low-Cost Housing Buildings: 9")
if __name__ == "__main__":
    unittest.main()
