import React from "react";
import { render, screen, cleanup } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Landing from "../Pages/Landing";
import Clinics from "../Pages/Clinics";
import SearchBar from "../Components/SearchBar";
import SearchResults from "../Pages/SearchResults";
import { ChakraProvider } from "@chakra-ui/react";
import bg1 from "../pictures/endless-constellation.svg";

// Cleaning up after each test
afterEach(cleanup);

describe("Landing component", () => {
    // Test 1: Ensuring the Landing component renders without any errors.
    test("1. Render test for Landing component", () => {
        render(<Landing />);
    });

    // Test 2: Verifying that the Landing page contains the specific text "New York Aid".
    test('2. Display the "New York Aid" text', () => {
        render(<Landing />);
        expect(screen.getByText("New York Aid")).toBeInTheDocument();
    });

    // Test 3: Ensuring that the tagline or slogan "Illuminating Paths, Changing Lives" is displayed.
    test('3. Display the tagline "Illuminating Paths, Changing Lives"', () => {
        render(<Landing />);
        expect(
            screen.getByText("Illuminating Paths, Changing Lives")
        ).toBeInTheDocument();
    });

    // Test 4: Checking the presence of a heading element on the Landing page.
    test("4. Check for the presence of a heading", () => {
        render(<Landing />);
        expect(screen.getByRole("heading")).toBeInTheDocument();
    });

    // Test 5: Verifying that the Landing component displays at least one image.
    test("5. Ensure an image is displayed on the Landing page", () => {
        render(<Landing />);
        expect(screen.getByRole("img")).toBeInTheDocument();
    });

    // Test 6: Confirming the alt text of the displayed image on the Landing page.
    test('6. Confirm the alt text "Landing Page Image" for the displayed image', () => {
        render(<Landing />);
        expect(screen.getByAltText("Landing Page Image")).toBeInTheDocument();
    });
});

describe("Clinics component", () => {
    // Test 1: Ensuring the Clinics component renders without any errors.
    test("1. Render test for Clinics component", () => {
        render(
            <ChakraProvider>
                <Clinics />
            </ChakraProvider>
        );
    });

    // Test 2: Verifying that the Clinics page contains the specific text "Clinics".
    test('2. Display the "Clinics" text', () => {
        render(
            <ChakraProvider>
                <Clinics />
            </ChakraProvider>
        );
        expect(screen.getByText("Clinics")).toBeInTheDocument();
    });

    // Test 3: Ensuring the grid layout for displaying clinics.
    test("3. Confirm the grid layout for displaying clinics", () => {
        render(
            <ChakraProvider>
                <Clinics />
            </ChakraProvider>
        );
        const cardGrid = screen.getByTestId("card-grid");
        expect(cardGrid).toHaveStyle("display: grid");
    });

    // Test 4: Check for the presence of expected VStack component.
    test("it has the intended background color", () => {
        render(
            <ChakraProvider>
                <Clinics />
            </ChakraProvider>
        );

        const vstackElement = screen.getByTestId("vstack-element");
        expect(vstackElement).toHaveAttribute("data-testid", "vstack-element");
    });

    // Test 5: Check for presence of the text "Number of Clinics:" followed by the actual number of clinics.
    test("it displays the number of clinics", () => {
        render(
            <ChakraProvider>
                <Clinics />
            </ChakraProvider>
        );
        const clinicsCountText = screen.getByText(/Number of Clinics:/i);
        expect(clinicsCountText).toBeInTheDocument();
    });

    // Test 6: Check for the search filter to have proper placeholder
    test("search filter has correct placeholder", () => {
        render(
            <ChakraProvider>
                <Clinics />
            </ChakraProvider>
        );
        const search = screen.getByTestId("search-input");
        expect(search).toHaveAttribute("placeholder", "Search clinics...");
    });
});

describe("Search", () => {
    // Test 1: Ensuring the Search bar component renders without any errors.
    test("1. Render test for Search bar component", () => {
        render(
            <ChakraProvider>
                <SearchBar />
            </ChakraProvider>
        );
    });

    // Test 2: Verifying that the Search page renders with proper style.
    test("2. Display the search bar properly", () => {
        render(
            <ChakraProvider>
                <SearchBar />
            </ChakraProvider>
        );
        const button = screen.getByTestId("search-bar");
        expect(button).toHaveStyle("marginRight: 2px");
    });

    // Test 3: Ensuring the search button style.
    test("3. Confirm the button style for the search bar", () => {
        render(
            <ChakraProvider>
                <SearchBar />
            </ChakraProvider>
        );
        const button = screen.getByTestId("search");
        expect(button).toHaveAttribute("aria-label", "Search");
    });

    // Test 4: Ensuring the grid layout for displaying search results.
    test("4. Confirm the grid layout for search results", () => {
        render(
            <ChakraProvider>
                <SearchResults />
            </ChakraProvider>
        );
        const cardGrid = screen.getByTestId("search-card-grid");
        expect(cardGrid).toHaveStyle("display: grid");
    });
});
