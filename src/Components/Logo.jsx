import React from "react";
import { HStack, Text } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

function Logo() {
    const navigate = useNavigate();
  
    return (
      <HStack
        as="nav"
        display={{ base: "none", md: "flex" }}
        justify="center"
        onClick={() => navigate("/")}
        p={8}
        cursor="pointer"
        _hover={{
          opacity: "0.8",
        }}
        style={{
          flexShrink: 1, // Allow the logo to shrink if necessary
        }}
      >
        <Text
          fontSize={{ base: "2xl", md: "3xl" }} // Responsive font size
          fontWeight="bold"
          color="white"
          className="font-face-gm"
          style={{
            whiteSpace: 'nowrap', // Prevent the text from wrapping
            overflow: 'hidden', // Hide overflow
            textOverflow: 'ellipsis' // Add an ellipsis if the text overflows
          }}
        >
          New York Aid
        </Text>
      </HStack>
    );
  }

export default Logo;
