import React from "react";
import { SimpleGrid } from "@chakra-ui/react";
import "./Components.css";
import { Center } from "@chakra-ui/react";

//Params: data: list of card data to render
//
//Notes: Simplegrid rows have more elements as the screen expands, to a max of 3.
function CardGrid(props) {
    return (
        <Center>
            <SimpleGrid
                data-testid={props["data-testid"]}
                columns={{ base: 1, sm: 2, md: 3 }}
                spacing="40px"
            >
                {props.children}
            </SimpleGrid>
        </Center>
    );
}

export default CardGrid;
