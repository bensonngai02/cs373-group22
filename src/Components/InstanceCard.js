import React from "react";
import {
    Card,
    CardHeader,
    CardBody,
    Heading,
    Text,
    LinkOverlay,
    Spacer,
    Image,
    VStack,
} from "@chakra-ui/react";
import "./Components.css";
import Map from "../Components/Map";

function DynamicCardBody(props) {
    let dataMapping = {};

    const getHighlightedText = (text, query) => {
        // Convert text to a string if it's not already one, and ensure query is a string to prevent errors.
        const safeText = String(text);
        const safeQuery = String(query);

        // If there's no query or the text is effectively empty, return the text or a placeholder.
        if (!safeQuery || !safeText.trim()) return safeText || "N/A";

        const parts = safeText.split(new RegExp(`(${safeQuery})`, "gi"));
        return parts.map((part, index) =>
            part.toLowerCase() === safeQuery.toLowerCase() ? (
                <mark key={index}>{part}</mark>
            ) : (
                part
            )
        );
    };

    if (props.type !== "text") {
        dataMapping = {
            clinic: [
                {
                    label: "Address",
                    value: getHighlightedText(
                        props.data.full_address,
                        props.searchQuery
                    ),
                },
                {
                    label: "Phone",
                    value: getHighlightedText(
                        props.data.fac_phone ? props.data.fac_phone : "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Zip Code",
                    value:
                        props.data.fac_zip != null
                            ? getHighlightedText(
                                  props.data.fac_zip.toString(),
                                  props.searchQuery
                              )
                            : "N/A",
                },
                {
                    label: "County",
                    value: getHighlightedText(
                        props.data.county,
                        props.searchQuery
                    ),
                },
                {
                    label: "Website",
                    value: getHighlightedText(
                        props.data.web_site ? props.data.web_site : "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Profit/Non-profit",
                    value: getHighlightedText(
                        props.data.ownership_type,
                        props.searchQuery
                    ),
                },
            ],
            housing: [
                {
                    label: "Project Name",
                    value: getHighlightedText(
                        props.data.project_name,
                        props.searchQuery
                    ),
                },
                {
                    label: "Address",
                    value: getHighlightedText(
                        props.data.full_address || "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Zip Code",
                    value: getHighlightedText(
                        props.data.zip_code?.toString() || "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Income Class",
                    value: getHighlightedText(
                        props.data.primary_income_class || "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Total Units",
                    value: props.data.total_units?.toString() || "N/A",
                },
                {
                    label: "Total Beds",
                    value: props.data.total_beds?.toString() || "N/A",
                },
                {
                    label: "Completed",
                    value:
                        props.data.is_completed?.toString().toUpperCase() ||
                        "N/A",
                },
            ],
            program: [
                {
                    label: "Program Name",
                    value: getHighlightedText(
                        props.data.program_name || "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Type",
                    value: getHighlightedText(
                        props.data.type || "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Address",
                    value: getHighlightedText(
                        props.data.address || "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Borough",
                    value: getHighlightedText(
                        props.data.borough || "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Phone",
                    value: getHighlightedText(
                        props.data.phone || "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "Zip Code",
                    value: getHighlightedText(
                        props.data.zip_code || "N/A",
                        props.searchQuery
                    ),
                },
                {
                    label: "NTA",
                    value: getHighlightedText(
                        props.data.nta || "N/A",
                        props.searchQuery
                    ),
                },
                // Include any other fields that need to be displayed
            ],
            text: [],
        };
    }

    return (
        <div>
            {/* if the type is not text and type is not clinic, then render the map */}
            {props.type !== "text" && (
                <VStack>
                    <Map
                        width="200"
                        height="200"
                        lat={props.data.latitude}
                        long={props.data.longitude}
                    />
                    <Spacer height="20px" />
                    {dataMapping[props.type].map((item, idx) => (
                        <Text key={idx}>
                            <span style={{ fontWeight: "bold" }}>
                                {item.label}:
                            </span>{" "}
                            {item.value}
                        </Text>
                    ))}
                </VStack>
            )}
            
            {/*if the type is text, then render the image*/}
            {props.type === "text" && (
                <VStack>
                    {props.image && (
                        <Image
                            src={props.image}
                            alt="Description of Image"
                            width="100"
                            height="100"
                        />
                    )}
                    {props.data}
                </VStack>
            )}
        </div>
    );
}

function InstanceCard(props) {
    return (
        <Card
            align={props.align}
            width={props.width}
            height={props.height}
            transition="all 0.3s"
            _hover={{
                boxShadow: "xl",
                transform: "scale(1.05)",
            }}
        >
            <CardHeader>
                <Heading size="lg">
                    <LinkOverlay href={props.link}>{props.title}</LinkOverlay>
                </Heading>
                <Heading size="sm" color={"#337CCF"}>
                    <LinkOverlay href={props.link}>
                        <i>{props.subtitle}</i>
                    </LinkOverlay>
                </Heading>
            </CardHeader>
            <CardBody>
                {props.type === "text" ? (
                    <DynamicCardBody
                        type={props.type}
                        data={props.children}
                        image={props.image}
                        imgSrc={props.img}
                    />
                ) : (
                    <DynamicCardBody
                        type={props.type}
                        data={props.data}
                        image={props.image}
                        imgSrc={props.img}
                        searchQuery={props.searchQuery} // Pass the searchQuery prop
                    />
                )}
            </CardBody>
        </Card>
    );
}

export { InstanceCard, DynamicCardBody };
