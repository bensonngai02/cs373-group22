import React from "react";
import {
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    IconButton,
    Box,
} from "@chakra-ui/react";
import { HamburgerIcon } from "@chakra-ui/icons";
import { useNavigate } from "react-router-dom";
import SearchBar from "./SearchBar";

function HamburgerMenu() {
    const navigate = useNavigate();

    return (
        <Box display={{ base: "block", md: "none" }}>
            <Menu>
                <MenuButton
                    as={IconButton}
                    aria-label="Options"
                    icon={<HamburgerIcon />}
                    variant="outline"
                    color="white"
                />
                <MenuList>
                    <MenuItem onClick={() => navigate("/about")}>
                        About
                    </MenuItem>
                    <MenuItem onClick={() => navigate("/clinics")}>
                        Healthcare Clinics
                    </MenuItem>
                    <MenuItem onClick={() => navigate("/housing")}>
                        Low-Cost Housing
                    </MenuItem>
                    <MenuItem onClick={() => navigate("/programs")}>
                        Benefits Programs
                    </MenuItem>
                    <SearchBar/>
                </MenuList>
            </Menu>
        </Box>
    );
}

export default HamburgerMenu;
