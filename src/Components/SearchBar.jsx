import React from "react";

import { Input, IconButton, HStack } from "@chakra-ui/react";
import { SearchIcon } from "@chakra-ui/icons";

const SearchBar = (props) => {
    const handleSearch = (e) => {
        e.preventDefault();
        const inputValue = e.target.search.value;
        window.location.href = `/search/${inputValue}`;
    };

    return (
        <form onSubmit={handleSearch} style={{ maxWidth: "100%" }}>
            <HStack>
                <Input
                    data-testid="search-bar"
                    name="search"
                    placeholder="Search"
                    color={"white"}
                    width={200}
                    style={{
                        flexGrow: 1, // Allow the input to grow and fill the available space
                        maxWidth: '100%', // Ensure the input doesn't exceed the navbar width
                      }}
                />
                <IconButton
                    data-testid="search"
                    type="submit"
                    aria-label="Search"
                    icon={<SearchIcon />}
                />
            </HStack>
        </form>
    );
};

export default SearchBar;
