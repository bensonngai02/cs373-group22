import React from "react";
import { Card, CardHeader, CardBody, Text, Heading, Image, Link, Box, Flex } from "@chakra-ui/react";
import "./Components.css";

function ProfileCard(props) {
    return(
        <Card direction={"row"} marginY={6}>
            <Box padding={2} display={"flex"} justifyContent={"center"} >
                <Image src={props.image} fallbackSrc='https://via.placeholder.com/150' width={175} height={175} objectFit="cover" objectPosition={"top center"}  borderRadius={15}/>
            </Box>

            <CardHeader>
                <Heading size='md'>{props.name}</Heading>
                <Heading size='sm' color={"#337CCF"}><i>{props.responsibilities}</i></Heading>
                <Link href={`mailto:${props.email}`}>{props.email}</Link>
            </CardHeader>
            
            <CardBody>
                <Text paddingBottom={5}>{props.bio}</Text>
                <Flex direction={"row"}>
                    <Text paddingEnd={12} ><span style={{fontWeight:"bold"}}>Commits:</span> {props.commits}</Text>
                    <Text paddingEnd={12}><span style={{fontWeight:"bold"}}>Issues:</span> {props.issues}</Text>
                    <Text paddingEnd={12}><span style={{fontWeight:"bold"}}>Unit Tests:</span> {props.tests}</Text>
                </Flex>
                
            </CardBody>
        </Card>
    );
}

export default ProfileCard;