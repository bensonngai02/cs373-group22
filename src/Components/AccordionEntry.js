import React from "react";
import { Box, Heading } from "@chakra-ui/layout";
import { AccordionItem, AccordionIcon, AccordionPanel, AccordionButton } from "@chakra-ui/accordion";

function AccordionEntry(props) {
    return(
        <AccordionItem>
            <AccordionButton>
                <Box as="span" flex='1' textAlign='left'>
                    <Heading size={"md"}>{props.title}</Heading>
                </Box>
                <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pb={4}>
                {props.text}
            </AccordionPanel>
        </AccordionItem>
    );
}

export default AccordionEntry;