import { Text } from "@chakra-ui/react";

const RippleText = (props) => {
    return (
        <Text
            {...props}
            mt={0} // Margin top set to zero
            mb={1} // Slightly adjusted margin-bottom
            position="relative"
            _before={{
                content: '""',
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                height: "100%",
                zIndex: 1,
                background: "black", // Match this with your background color
                animation: "ripple 1s forwards",
                animationDelay: "0.5s", // Adjust as needed
            }}
        >
            {props.children}
        </Text>
    );
};

export default RippleText;
