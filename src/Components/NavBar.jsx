import React from "react";
import {
    Box,
    useColorModeValue,
    Flex,
    useMediaQuery,
} from "@chakra-ui/react";
import Logo from "../Components/Logo";
import NavLinks from "../Components/NavLinks";
import SearchBar from "../Components/SearchBar";
import HamburgerMenu from "../Components/HamburgerMenu"; 

function NavBar() {
    const [isLargerThan768] = useMediaQuery("(min-width: 768px)");

    return (
        <Box
            bg={useColorModeValue("black", "gray.800")}
            px={4}
            py={2}
            style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
            }}
        >
            <Flex
                align="center"
                mr={5}
            >
                <Logo />
            </Flex>
            <Flex
                justify="space-between"
                align="center"
                flex="1"
                minW="0" // Ensures the flex child doesn't force itself to fit its content
            >
                {isLargerThan768 ? <NavLinks /> : <HamburgerMenu />}
                <SearchBar />
            </Flex>
        </Box>
    );
}

export default NavBar;
