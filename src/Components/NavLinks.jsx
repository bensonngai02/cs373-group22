import React from "react";
import { Button, Box } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
// import SearchBar from "./SearchBar";

function NavLinks() {
    const navigate = useNavigate();

    const navButton = (label, route) => (
        <Button
            variant="ghost"
            size="md"
            fontSize="md"
            onClick={() => navigate(route)}
            color="white"
            _hover={{
                color: "black",
                bg: "white",
                transform: "scale(1.05)",
                transition: "all 0.2s",
            }}
            transition="all 0.2s"
        >
            {label}
        </Button>
    );

    return (
        <Box spacing={4}>
            {navButton("About", "/about")}
            {navButton("Healthcare Clinics", "/clinics")}
            {navButton("Low-Cost Housing", "/housing")}
            {navButton("Benefits Programs", "/programs")}
            {navButton("Visualizations", "/visualizations")}
            {navButton("Provider Visualizations", "/provider_visualizations")}
        </Box>
    );
}

export default NavLinks;
