import React from "react";

const EMBED_API_KEY = "AIzaSyB2sDS6_QxF2Awf3dtkWhxwRLj6ByiGkyE";

const Map = (props) => {
    let lat_long = encodeURIComponent(`${props.lat},${props.long}`);
    return (
        <iframe
            title={lat_long}
            width={props.width}
            height={props.height}
            frameBorder="0" style={{"border":"0"}}
            referrerPolicy="no-referrer-when-downgrade"
            src={`https://www.google.com/maps/embed/v1/view?key=${EMBED_API_KEY}&center=${lat_long}&zoom=18`}
            allowFullScreen>
        </iframe>
    );
}

export default Map;
