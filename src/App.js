import "./App.css";
import * as React from "react";
// import needed for navigation
import { BrowserRouter, Routes, Route } from "react-router-dom";
// import for css framework
import { ChakraProvider } from "@chakra-ui/react";
import { GlobalStyles } from "./GlobalStyles";
// imports for components
import NavBar from "./Components/NavBar";
import About from "./Pages/About";
import Landing from "./Pages/Landing";
import Clinics from "./Pages/Clinics";
import Housing from "./Pages/Housing";
import Programs from "./Pages/Programs";
import PageNotFound from "./Pages/PageNotFound";
import ClinicInstance from "./Pages/ClinicInstance";
import HousingInstance from "./Pages/HousingInstance";
import ProgramInstance from "./Pages/ProgramInstance";
import SearchResults from "./Pages/SearchResults";
import Visualizations from "./Pages/Visualizations";
import ProviderVisualizations from "./Pages/ProviderVisualizations";

function App() {
    return (
        <ChakraProvider>
            <GlobalStyles />
            <BrowserRouter>
                <NavBar />
                <Routes>
                    {/* route to landing page */}
                    <Route path="/" element={<Landing />} />
                    {/* route to about page */}
                    <Route path="about" element={<About />} />
                    {/* route to model 1 (Clinics)*/}
                    <Route path="clinics" element={<Clinics />} />
                    {/* route to model 2 (Low-Cost Housing*/}
                    <Route path="housing" element={<Housing />} />
                    {/* route to model 3 (Benefits Programs)*/}
                    <Route path="programs" element={<Programs />} />
                    {/* PAGE DOES NOT EXIST*/}
                    <Route path="" element={<PageNotFound />} />
                    {/* route to clinic instances */}
                    <Route
                        path="clinic_instance/:idx"
                        element={<ClinicInstance />}
                    />
                    {/* route to housing instances */}
                    <Route
                        path="housing_instance/:idx"
                        element={<HousingInstance />}
                    />
                    {/* route to program instances */}
                    <Route
                        path="program_instance/:idx"
                        element={<ProgramInstance />}
                    />
                    {/* route to search results */}
                    <Route path="search/:search?" element={<SearchResults />} />
                    {/* route to visualizations */}
                    <Route path="visualizations" element={<Visualizations />} />
                    {/* route to provider visualizations */}
                    <Route
                        path="provider_visualizations"
                        element={<ProviderVisualizations />}
                    />
                </Routes>
            </BrowserRouter>
        </ChakraProvider>
    );
}

export default App;
