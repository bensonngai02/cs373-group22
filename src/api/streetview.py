import google_streetview.api
import pandas as pd
import os
import shutil
from models import Base, Clinic, Housing, Program
import cv2
import base64

#move file out of parent directory, up 1 level. With {} as directory, {A}/{B}/C becomes {A}/C
def move_up(current_path, file_to_move, new_filename):

    # Calculate the new path (one level up)
    new_path = '/'.join(current_path.split('/')[:-1])
    new_file = os.path.join(new_path, new_filename)
    # Move the file to the new path
    shutil.move(os.path.join(current_path, file_to_move), new_file)

    #clear the folder and contents
    os.remove(os.path.join(current_path, 'metadata.json'))
    os.rmdir(current_path)

    return new_file

def base64_encode_image(image_path) -> str :

    image = cv2.imread(image_path)

    # Encode the image as a base64 string
    ret, buffer = cv2.imencode('.jpg', image)
    base64_image = "NO IMAGE"
    if ret:
        base64_image = base64.b64encode(buffer).decode('utf-8')
    return base64_image

def get_image(latitude, longitude, id, download_dir, filename_template, cached=True):

    if cached:
        test_path = os.path.join(download_dir, filename_template+str(id)+".jpg")
        print(test_path)
        if os.path.exists(test_path):
            print("HIT")
            return test_path
        
    print("MISS")

    params = [{
            'size': '600x300', # max 640x640 pixels
            'location': ''+str(latitude)+','+str(longitude), #'40.687092,-73.97654',
        #	'heading': '180',
            'pitch': '0',
            'key': 'AIzaSyDtwmL8YcPcUlaovVkFUu-c0O4g7JkWjKM'
        }]
    
        # Create a results object
    results = google_streetview.api.results(params)

    output_filename  = filename_template+str(id)
    results.download_links(download_dir+output_filename)

    #new location of image after download
    image_path = move_up(download_dir+output_filename, 'gsv_0.jpg', output_filename+".jpg")

    return image_path


def get_images_for_table(session, table, download_dir, filename_template):

    for i, record in enumerate(table[:3]):
        latitude = record.latitude
        longitude = record.longitude
       # print(latitude, longitude)
        img_path =   get_image(latitude, longitude, i+1, download_dir, filename_template)
        img_string = base64_encode_image(img_path)
        record.image = img_string
       # print(img_path, img_string,"...")

    session.commit() 
    # clinics = pd.read_json("src/api/scraped/clinics4.json")
    # clinics = clinics[:2]
    # assert len(clinics) == 2

    # for i, row in clinics.iterrows():
    