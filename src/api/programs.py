from flask import request, jsonify, Blueprint
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
from models import Program

AWS_DB_HOST = "swe-group-22-db.ccydffu4rlbz.us-east-2.rds.amazonaws.com"
AWS_DB_USERNAME = "admin"
AWS_DB_PASSWORD = "new-york-aid-db"
DB_NAME = "swe_group_22_db"
DB_URL = "mysql+mysqlconnector://" + AWS_DB_USERNAME + ":" + AWS_DB_PASSWORD + "@" + AWS_DB_HOST + "/" + DB_NAME

# blueprint = Blueprint('programs', __name__)
# engine = create_engine(DB_URL, echo=True)
# Session = sessionmaker(bind=engine)
# session = Session()

# @blueprint.route('/api/getAllPrograms', methods=['GET'])
# def get_all_programs():
#     """
#     get stuff from the database
#     return stuff from the database
#     """
#     query = session.query(Program)
#     params = request.args

#     if params:
#         borough = request.args.get('borough')
#         zipcode = request.args.get('zipcode')
#         type = request.args.get('type')
#         nta = request.args.get('nta')
#         bbl = request.args.get('bbl')
#         bin = request.args.get('bin')
#         censusTract = request.args.get('censusTract')
#         communityBoard = request.args.get('communityBoard')
#         communityCouncil = request.args.get('communityCouncil')

#         if borough: query = query.filter(Program.borough == borough)
#         if zipcode: query = query.filter(Program.zip_code == zipcode) 
#         if type: query = query.filter(Program.type == type)
#         if nta: query = query.filter(Program.nta == nta) 
#         if bbl: query = query.filter(Program.bbl == bbl)
#         if bin: query = query.filter(Program.bin == bin) 
#         if censusTract: query = query.filter(Program.census_tract == censusTract)
#         if communityBoard: query = query.filter(Program.community_board == communityBoard) 
#         if communityCouncil: query = query.filter(Program.community_council == communityCouncil) 


#     results = query.all()
#     results_dict = [result.__dict__ for result in results]
#     for item in results_dict:
#         item.pop("_sa_instance_state", None)
#     print(results_dict)
#     # return response as json
#     return jsonify(results_dict)

# @blueprint.route('/api/getProgram/<int:program_id>', methods = ['GET'])
# def get_program(program_id):
#     # use the program_id passed in by user to fetch program with matching unique identifier
#     # Query the database using the provided ID
#     result = session.query(Program).filter(Program.id == program_id).first()

#     if result:
#         # Convert the result to a dictionary
#         result_as_dict = result.__dict__
#         # Remove the _sa_instance_state attribute
#         result_as_dict.pop('_sa_instance_state', None)
#         # Return the result as JSON
#         return jsonify(result_as_dict)
#     else:
#         return jsonify({'message': 'Entry not found'}), 404


