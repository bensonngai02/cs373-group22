import pandas as pd
from sodapy import Socrata

MAX_RECORDS_TO_RETURN = 30
NUM_RECORDS_TO_TAKE = 3
# APP_TOK = "UIdgXLDj89MAcXeAVblKyyVWi"

# Unauthenticated client only works with public data sets. Note 'None'
# in place of application token, and no username or password:
client = Socrata("health.data.ny.gov", None)

# Example authenticated client (needed for non-public datasets):
# client = Socrata(health.data.ny.gov,
#                  MyAppToken,
#                  username="user@example.com",
#                  password="AFakePassword")

# First 100 results, returned as JSON from API / converted to Python list of
# dictionaries by sodapy.
results = client.get("vn5v-hh5r", limit=MAX_RECORDS_TO_RETURN)

# Convert to pandas DataFrame
df = pd.DataFrame.from_records(results)

#select first N elements from brooklyn
filtered = df[df["city"] == "Brooklyn"][:NUM_RECORDS_TO_TAKE]
print(filtered.head())

#write to file
filtered.to_json("src/api/scraped/clinics.json", indent=2, orient="records")

df["address"] = "" + df["address1"] + ", " + df["city"] + ", " + df["state"]  + ", " + df["fac_zip"]            
df.drop["address"]    
df2 = df[df["city"].isin(["New York", "Bronx","Queens","Brooklyn","Manhattan","Staten Island"])]
df2 = df.dropna(subset=["latitude","longitude"])       


