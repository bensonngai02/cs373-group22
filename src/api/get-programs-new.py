#!/usr/bin/env python

#https://data.cityofnewyork.us/resource/kvhd-5fmu.json

import pandas as pd
from sodapy import Socrata
import json
import re

MAX_RECORDS_TO_RETURN = 30
NUM_RECORDS_TO_TAKE = 3

# Unauthenticated client only works with public data sets. Note 'None'
# in place of application token, and no username or password:
client = Socrata("data.cityofnewyork.us", None)

# Example authenticated client (needed for non-public datasets):
# client = Socrata(data.cityofnewyork.us,
#                  MyAppToken,
#                  username="user@example.com",
#                  password="AFakePassword")

# First 2000 results, returned as JSON from API / converted to Python list of
# dictionaries by sodapy.
homeless_drop_in = client.get("bmxf-3rd4")
benefits_access_centers = client.get("9d9t-bmk7")
snap_kitchens = client.get("tc6u-8rnp")
homebase = client.get("ntcm-2w4k")
youth = client.get("ujsc-un6m")


# # Convert to pandas DataFrame
df_homeless = pd.read_json(json.dumps(homeless_drop_in, indent=2), orient="records")
df_homeless.rename(columns={'center_name':'program_name'}, inplace=True)

df_benefits = pd.read_json(json.dumps(benefits_access_centers, indent=2), orient="records")
df_benefits.rename(columns={'facility_name':'program_name'}, inplace=True)

df_snap = pd.read_json(json.dumps(snap_kitchens, indent=2), orient="records")
df_snap.rename(columns={'facility_name':'program_name'}, inplace=True)

df_homebase = pd.read_json(json.dumps(homebase, indent=2), orient="records")
df_homebase.rename(columns={'provider':'program_name'}, inplace=True)

df_youth = pd.read_json(json.dumps(youth, indent=2), orient="records")
df_youth.rename(columns={'site_name':'program_name'}, inplace=True)


# write to file
df_homeless.to_json("src/api/scraped/homeless-drop-in.json", indent=2, orient="records")
df_benefits.to_json("src/api/scraped/benefits-access.json", indent=2, orient="records")
df_snap.to_json("src/api/scraped/snap-kitchens.json", indent=2, orient="records")
df_homebase.to_json("src/api/scraped/homebase.json", indent=2, orient="records")
df_youth.to_json("src/api/scraped/youth.json", indent=2, orient="records")



