import math, requests, os
from dotenv import load_dotenv

load_dotenv()

EARTH_RADIUS = 3958.8
GOOGLE_MAPS_API_KEY = os.getenv("GOOGLE_MAPS_API_KEY")

def get_distance(coord1 : tuple[float, float], coord2 : tuple[float, float]) -> float:
  #  print(coord1,coord2)
    """
    This function implements the Haversine distance,
    which takes in coord1 : (latitude1, longitude1) and
    coord2: (latitude2, longitutde 2) and returns distance in miles
    """
    lat1, long1 = tuple(map(math.radians, coord1))
    lat2, long2 = tuple(map(math.radians, coord2))

    d_lat = lat2 - lat1
    d_long = long2 - long1

    a = math.sin(d_lat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(d_long / 2) ** 2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))

    total_dist = EARTH_RADIUS * c
    return total_dist


def get_coords(address: str) -> tuple[float, float]:
    """
    This function accepts a string address and returns the corresponding
    (latitude, longitude) coordinates using the Google Maps API. 

    Addresses should be passed in as strings in the form
    [StreetAddress][City][State], such as:
        35 1st Street Manhattan New York

    There IS a quota for performing requests to not get charged.
    Google Maps gives us $200/month, which should be enough for
    40,000 requests per month. 

    [EXAMPLE USAGE]
        ADDRESS = "110 Inner Campus Drive Austin TX 78705"
        coordinates = get_coords(ADDRESS)
        if coordinates:
            print(f"Latitude: {coordinates[0]}, Longitude: {coordinates[1]}")
        else:
            print("Geocoding failed.")
    [EXPECTED RESULT]
        Latitude: 30.2861062, Longitude: -97.7393634
        
    More resources here:
    https://developers.google.com/maps/documentation/geocoding/usage-and-billing
    https://developers.google.com/maps/documentation/geocoding/requests-geocoding
    """
    base_url = "https://maps.googleapis.com/maps/api/geocode/json"
    params = {
        "address": address,
        "key": GOOGLE_MAPS_API_KEY,
    }

    timeout = 10
    response = requests.get(base_url, params=params, timeout=timeout)
    data = response.json()

    if data.get("status") == "OK" and data.get("results"):
        location = data["results"][0]["geometry"]["location"]
        latitude = location["lat"]
        longitude = location["lng"]
        return (latitude, longitude)
    else:
        return None