import mysql.connector, os, json
from dotenv import load_dotenv
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker
from models import Base, Clinic, Housing, Program, HousingLinks, ClinicLinks, ProgramLinks
from knn import knn

load_dotenv()

AWS_DB_HOST = os.getenv("DB_HOST")
AWS_DB_USERNAME = os.getenv("DB_USERNAME")
AWS_DB_PASSWORD = os.getenv("DB_PASSWORD")

DB_NAME = "swe_group_22_db"
DB_URL = "mysql+mysqlconnector://" + AWS_DB_USERNAME + ":" + AWS_DB_PASSWORD + "@" + AWS_DB_HOST + "/" + DB_NAME

def create_connection():
    """
    Establishes a connection to the databse hosted on Amazon
    Relational Database Service (RDS)
    Max. 750 free hours before we get charged.
    """
    connection = mysql.connector.connect(
        host = AWS_DB_HOST,
        user = AWS_DB_USERNAME,
        password = AWS_DB_PASSWORD,
        database = DB_NAME,
        port=3306
    )

    return connection

engine = create_engine(DB_URL, echo=True)
Session = sessionmaker(bind=engine)
session = Session()

def create_table():
    try:
        connection = create_connection()
        cursor = connection.cursor()

        #use only if we have to nuke the db
        #cursor.execute("DROP DATABASE IF EXISTS swe_group_22_db")

        cursor.execute("CREATE DATABASE IF NOT EXISTS swe_group_22_db")

        connection.commit()
        Base.metadata.create_all(engine)

        cursor.close()
    finally:
        connection.close()

def add_entries(file_path_json, *, T):

    with open(file_path_json, 'r') as file:
        data = json.load(file)

    model_attributes = {k: v for k, v in vars(T).items() if not k.startswith('_') and not k == 'id' and not any(check in k for check in ["housing_inst","clinic_inst","program_inst"])}

    for entry in data:
        instance_attributes = {}
        for attribute in model_attributes.keys():
            instance_attributes[attribute] = entry.get(attribute)
        new_instance = T(**instance_attributes)
        session.add(new_instance)
    session.commit()


def build_database():

    #fill db with entries
    add_entries('src/api/scraped/all-programs2.json', T=Program)
    add_entries('src/api/scraped/clinics4.json', T=Clinic)
    add_entries('src/api/scraped/housing3.json', T=Housing)

    # clinics  = session.query(Clinic).all()
    # housing = session.query(Housing).all()
    # programs = session.query(Program).all()

    # k = 3

    #for each element in 'from', finds the k nearest elements of 'to'
    #and places their ids in the specified columns of the 'from' table
    #
    # ex: connect_instances(housing, programs, "hello", 3) will place the 3 closest
    # programs to each house under housing.hello1, housing.hello2, etc
    #
    # def connect_instances(list_from, list_to, column_name, k=3):

    #     knn_return = knn(list_from, list_to, k)
        
    #     neighbors = iter(knn_return)
    #     #print(len(neighbors), len(clinics))
    #     for i, record in enumerate(list_from):
    #         nearest_neighbors = next(neighbors)
    #         for inst in range(k):
    #             record.__setattr__(column_name+str(inst+1), nearest_neighbors[inst])

   
        # for i, record in enumerate(list_from):
        #     nearest_neighbors = next(neighbors)


    #connect associated instances
    # connect_instances(housing,  programs, column_name="program_inst",  k=3)
    # connect_instances(clinics,  programs, column_name="program_inst",  k=3)
    # connect_instances(clinics,  housing, column_name="housing_inst", k=3)
    # connect_instances(programs, housing, column_name="housing_inst", k=3)
    # connect_instances(housing,  clinics, column_name="clinic_inst",  k=3)
    # connect_instances(programs, clinics, column_name="clinic_inst",  k=3)

    #def get_k_closest(source=housing, dest=[programs])

    # from streetview import get_images_for_table
    # print("/////////////////////")#,clinics)
    # get_images_for_table(session, clinics, download_dir="src/api/images/clinic/", filename_template="clinic-")

def build_link_tables():
    
    def getknndict(list_from, list_to, column_name, k=3):

        neighbors = knn(list_from, list_to, k)
        
        ret = []

        for nearest_neighbors in neighbors:
            links_dict = {column_name+str(inst+1) : nearest_neighbors[inst] for inst in range(k)}
            ret.append(links_dict)

        print(ret)
        return ret

    clinics  = session.query(Clinic).all()
    housing = session.query(Housing).all()
    programs = session.query(Program).all()

    k = 3

    #housing
    housing2program = getknndict(housing, programs, column_name="program_inst", k=3)
    housing2clinics = getknndict(housing, clinics,  column_name="clinic_inst", k=3)

    #make it a generator! 
    hlinks = (HousingLinks(id=id+1, **a, **b) for id, a, b in zip(range(len(housing)), housing2program, housing2clinics))
    session.add_all(hlinks)

    #-----------------------
    #programs
    program2housing = getknndict(programs, housing, column_name="housing_inst", k=3)
    program2clinics = getknndict(programs, clinics,  column_name="clinic_inst", k=3)

    p_links = (ProgramLinks(id=id+1, **a, **b) for id, a, b in zip(range(len(programs)), program2housing, program2clinics))
    session.add_all(p_links)

    #-----------------------  
    #clinics
    clinic2programs = getknndict(clinics, programs, column_name="program_inst", k=3)
    clinic2housing = getknndict(clinics, housing,  column_name="housing_inst", k=3)

    c_links = (ClinicLinks(id=id+1, **a, **b) for id, a, b in zip(range(len(clinics)), clinic2programs, clinic2housing))
    session.add_all(c_links)
    

if __name__ == "__main__":
        try:
            connection = create_connection()
            cursor = connection.cursor()

            cursor.execute("ALTER TABLE programs ADD FULLTEXT INDEX FTEXT(program_name, type, address, borough, phone, nta, comments)")


            # create_table()
            # #build_database()
            # cursor.execute("UPDATE housing SET zip_code = SUBSTR(full_address, -5);")
            session.commit()
            connection.commit()

            
            # #build_link_tables()
            # session.commit()

            # #--------TESTING--------
            # q = session.query(Housing).first()
            # print(q.__dict__)
            # print("--------------------------------------------")
            # q = session.query(HousingLinks).first()
            # print("---------------")
            # print(q)
            # print("_---------------")
            # # engine.execute(sql)
            # id = 1
            # a = session.query(Housing, HousingLinks).join(HousingLinks, HousingLinks.id == Housing.id).filter(Housing.id == id).first() #HousingLinks.id == id).all() #engine.execute("SELECT * FROM housinglinks LIMIT 3")
            # print(a)
            # result_as_dict = {**a[0].__dict__, **a[1].__dict__}
     
            # result_as_dict.pop('_sa_instance_state', None)
            # result_as_dict.pop('IGNORE', None)

            # print(result_as_dict)
  
            # session.commit()
            # connection.commit()

        finally:
            connection.close()

