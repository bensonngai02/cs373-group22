import pandas as pd
from utils import get_distance
import math

def knn(list_from, list_to, k):

    class Pair:
        def __init__(self, id, idx, dist):
            self.id  = id
            self.idx = idx
            self.dist = dist

        def __str__(self):
            return f"[d:"+ str(self.dist)+ " i:" + str(self.idx) + "]"

    all_items = [] 
    nan_ct = 0
    for i, tgt in enumerate(list_from):
       # print("--------------")
        #print(tgt["full_address"])
        neighbors = []
        for j, dest in enumerate(list_to):
            if i == j:
                continue
            # print(tgt, dest)
            dist = get_distance((tgt.latitude,tgt.longitude),
                                (dest.latitude,dest.longitude))
            neighbors.append(Pair(dest.id, j, dist))

        neighbors = [x for x in neighbors if not math.isnan(x.dist)]
        neighbors.sort(key=lambda x : x.dist)
        
        neighbors = neighbors[:k]
        if len(neighbors) < k:
            neighbors += ["N/A"] * (len(neighbors) - k) #fill in values if Nan... should never happen
        neighbors = neighbors[:k]

        all_items.append([x.id for x in neighbors])

        if not len(neighbors) >= 3:
            nan_ct += 1

    #print(nan_ct)

    return all_items

#DF VERSION
# def knn(df_from, df_to, k, name):

    

#     class Pair:
#         def __init__(self, idx, dist):
#             self.idx = idx
#             self.dist = dist

#         def __str__(self):
#             return f"[d:"+ str(self.dist)+ " i:" + str(self.idx) + "]"

#     all_items = [] 
#     nan_ct = 0
#     for i, tgt in df_from.iterrows():
#         print("--------------")
#         # print(tgt["full_address"])
#         neighbors = []
#         for j, dest in df_to.iterrows():
#             if i == j:
#                 continue
#             dist = get_distance((tgt["latitude"],tgt["longitude"]),
#                                 (dest["latitude"],dest["longitude"]))
#             neighbors.append(Pair(j, dist))

#         neighbors = [x for x in neighbors if not math.isnan(x.dist)]
#         neighbors.sort(key=lambda x : x.dist)
        
#         neighbors = neighbors[:k]
#         if len(neighbors) < k:
#             neighbors += ["N/A"] * (len(neighbors) - k) #fill in values\
#         neighbors = neighbors[:k]

#         all_items.append(neighbors)

#         # for x in neighbors[:k]:
#         #     print(x)
#         #     print(df_to["full_address"].iloc[x.idx])

#         if not len(neighbors) >= 3:
#             nan_ct += 1

#     print(nan_ct)
#     # print(all_items)
#     # , columns=[name+"_"+str(i) for i in range(1,k+1)
#     df = pd.DataFrame(all_items)
#     print(df)
#     # df = df.transpose()
#     # print(df.head())

# if __name__ == "__main__":
#     df_clinics  = pd.read_json("src/api/scraped/clinics3.json") 
#     df_housing  = pd.read_json("src/api/scraped/housing3.json") 
#     df_programs = pd.read_json("src/api/scraped/all-programs.json") 
#     knn(df_from=df_clinics, df_to=df_housing, k=3, name="a")      
            
            












#     latitude = df.columns.get_loc("latitude")
#     longitude = df.columns.get_loc("longitude")
   
#     def a_get_distance(x,y):
        

#         return get_distance((x[latitude],x[longitude]),
#                             (y[latitude],y[longitude]))
#         # return get_distance((x["latitude"],x["longitude"]),
#         #                     (y["latitude"],y["longitude"]))
    

#     knn = NearestNeighbors(n_neighbors=k, metric=a_get_distance)
#     print(df.values)
#     knn.fit(df[["latitude","longitude"]].values)

#     for index, row in df.iterrows():
#         print(knn.kneighbors([row]))

#    # df.apply(lambda x : find_knn(,k,df))

# clinics_knn(3)