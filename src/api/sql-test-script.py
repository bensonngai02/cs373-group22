"""
This is a quick testing script to ensure your environment is set up correctly
for MySQL querying.
"""
import os
from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Clinic, Housing, Program

load_dotenv()

AWS_DB_HOST = os.getenv("DB_HOST")
AWS_DB_USERNAME = os.getenv("DB_USERNAME")
AWS_DB_PASSWORD = os.getenv("DB_PASSWORD")

DB_NAME = "swe_group_22_db"
DB_URL = "mysql://" + AWS_DB_USERNAME + ":" + AWS_DB_PASSWORD + "@" + AWS_DB_HOST + "/" + DB_NAME

table_name_to_check = 'programs'
import mysql.connector
connection = mysql.connector.connect(
        host = AWS_DB_HOST,
        user = AWS_DB_USERNAME,
        password = AWS_DB_PASSWORD,
        database = DB_NAME,
        port=3306
    )

def table_exists(table_name):
    try:
        cursor = connection.cursor()
        cursor.execute(f"SHOW TABLES LIKE '{table_name}'")
        result = cursor.fetchone()
        cursor.close()
        return result is not None
    except mysql.connector.Error as err:
        print(f"Error: {err}")

def get_table_columns(table_name):
    try:
        cursor = connection.cursor()
        cursor.execute(f"DESCRIBE {table_name}")
        columns = [column[0] for column in cursor.fetchall()]
        cursor.close()
        return columns
    except mysql.connector.Error as err:
        print(f"Error: {err}")
    

# Usage
if table_exists(table_name_to_check):
    print(f"The table {table_name_to_check} exists.")
else:
    print(f"The table {table_name_to_check} does not exist.")

columns = get_table_columns(table_name_to_check)
print(f"Columns in the '{table_name_to_check}' table: {', '.join(columns)}")


cursor = connection.cursor()

cursor.execute(f"SELECT * FROM clinics")
rows = cursor.fetchall()
print(rows)
for row in rows:
    print(row)

engine = create_engine(DB_URL)
Session = sessionmaker(bind=engine)
session = Session()
query = session.query(Program)

print(query)
print('1')
print('2')
print(query.all())
connection.close()