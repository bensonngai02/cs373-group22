#!/usr/bin/env python

#https://data.cityofnewyork.us/resource/kvhd-5fmu.json

import pandas as pd
from sodapy import Socrata
import json
import re

MAX_RECORDS_TO_RETURN = 30
NUM_RECORDS_TO_TAKE = 3

# Unauthenticated client only works with public data sets. Note 'None'
# in place of application token, and no username or password:
client = Socrata("data.cityofnewyork.us", None)

# Example authenticated client (needed for non-public datasets):
# client = Socrata(data.cityofnewyork.us,
#                  MyAppToken,
#                  username="user@example.com",
#                  password="AFakePassword")

# First 2000 results, returned as JSON from API / converted to Python list of
# dictionaries by sodapy.
results = client.get("kvhd-5fmu", limit=10)

# Convert to pandas DataFrame
df = pd.read_json(json.dumps(results,indent=2), orient="records")

def remove_p_tags(s:str):
    pattern = r'(<p>)|(</p>)'
    split = re.split(pattern, s)
    result = [x  for x in split if x not in [None, "NULL", "<p>", "</p>", ""]]
    return result

#Text formatting
df["program_description"] = df["program_description"].apply(remove_p_tags)
df["brief_excerpt"] = df["brief_excerpt"].apply(remove_p_tags)
df["program_acronym"] = df["program_acronym"].replace("NULL", "")

#select first N elements
filtered = df.iloc[:NUM_RECORDS_TO_TAKE]
print(filtered.head())

#write to file
filtered.to_json("src/api/scraped/programs.json", indent=2, orient="records")