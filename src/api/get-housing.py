import pandas as pd
from sodapy import Socrata
import json
import numpy as np
import datetime
import pytz

MAX_RECORDS_TO_RETURN = 30
NUM_RECORDS_TO_TAKE = 3
# APP_TOK = "UIdgXLDj89MAcXeAVblKyyVWi"

# Unauthenticated client only works with public data sets. Note 'None'
# in place of application token, and no username or password:
client = Socrata("data.cityofnewyork.us", None)

# Example authenticated client (needed for non-public datasets):
# client = Socrata(health.data.ny.gov,
#                  MyAppToken,
#                  username="user@example.com",
#                  password="AFakePassword")

# First 100 results, returned as JSON from API / converted to Python list of
# dictionaries by sodapy.
results = client.get("hg8x-zxpr", limit=MAX_RECORDS_TO_RETURN)

# Convert to pandas DataFrame
df = pd.read_json(json.dumps(results,indent=2), orient="records")



#----------------------------------------------
#get majority unit cost
columns = ["extremely_low_income_units", "very_low_income_units", "low_income_units", "moderate_income_units","middle_income_units","other_income_units"]
df["primary_income_class"] = df[columns].idxmax(axis=1) #which column has the highest count?
print(df.head())

#Format
correction_map = {"extremely_low_income_units":"Extremely Low Income Units", "very_low_income_units":"Very Low Income Units", "low_income_units":"Low Income Units", "moderate_income_units":"Moderate Income Units","middle_income_units":"Middle Income Units","other_income_units":"Other Income Units"}
df["primary_income_class"] = df["primary_income_class"].map(correction_map)

#----------------------------------------------
#get total beds
columns = ["_1_br_units","_2_br_units","_3_br_units","_4_br_units","_5_br_units","_6_br_units"]
df["total_beds"] = df[columns].dot([1,2,3,4,5,6])
print(df["total_beds"].head())

#----------------------------------------------
#get largest room per building
columns = ["_1_br_units","_2_br_units","_3_br_units","_4_br_units","_5_br_units","_6_br_units"]

def largest_nonzero_index(col):
    nonzero_indices = np.where(col != 0)[0]
    if len(nonzero_indices) > 0:
        return max(nonzero_indices) + 1
    else:
        return -1  # or any other value to indicate no nonzero elements

# Apply the custom function to each column
df["largest_room"] = df[columns].apply(largest_nonzero_index, axis=1)

#----------------------------------------------
#format time UNUSED...it reads the timestamps as floats

def format_time(timestamp:str):
    if timestamp == "" or timestamp == None or timestamp == "nan":
        return "N/A"
    
    dt = datetime.datetime.strptime(str(timestamp), "%Y-%m-%dT%H:%M:%S.%f")
    # Format the datetime object as DDMMYY
    formatted_date = dt.strftime("%d%m%y")
    return formatted_date

print(df["building_completion_date"].dtype)
df["building_completion_date"] = df["building_completion_date"].astype(str)
df["formatted_building_completion_date"] = df["building_completion_date"].apply(format_time)

#----------------------------------------------
df["is_completed"] = df["building_completion_date"] != "nan"



#df["largest_room"] = df[columns].apply(lambda col: np.argmax(col.to_numpy() != 0))

#select first 5 elements from brooklyn
filtered = df[df["borough"] == "Brooklyn"][:NUM_RECORDS_TO_TAKE]
print(filtered.head())

#write to file
filtered.to_json("src/api/scraped/housing.json", indent=2, orient="records")

df = df.dropna(subset=['postcode'])
df["full_address"] = df["house_number"] + " " + df["street_name"] + ", " + df["borough"] + ", NYC, " + df["postcode"].astype(int).astype(str)