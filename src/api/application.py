from flask import Flask, request, jsonify
from flask_cors import CORS
import sqlalchemy as sql
from sqlalchemy.sql import func, text
from sqlalchemy import create_engine, select, case
from sqlalchemy.sql import func
from sqlalchemy.orm import sessionmaker
import sys

from models import Program, Clinic, Housing, HousingLinks, ClinicLinks, ProgramLinks

# from housing import blueprint as housing_blueprint
# from programs import blueprint as programs_blueprint
# from clinics import blueprint as clinics_blueprint

AWS_DB_HOST = "swe-group-22-db.ccydffu4rlbz.us-east-2.rds.amazonaws.com"
AWS_DB_USERNAME = "admin"
AWS_DB_PASSWORD = "new-york-aid-db"
DB_NAME = "swe_group_22_db"
DB_URL = "mysql+mysqlconnector://" + AWS_DB_USERNAME + ":" + AWS_DB_PASSWORD + "@" + AWS_DB_HOST + "/" + DB_NAME

application = Flask(__name__)
application.config['SQLALCHEMY_DATABASE_URI'] = DB_URL
CORS(application)

engine = create_engine(DB_URL, echo=True)
Session = sessionmaker(bind=engine)

@application.route('/api')
@application.route('/api/')
def home_page():
    return "Welcome to the New York Aid API!"

# Housing API
housingAttributeMap = {
    "totalUnits" : Housing.total_units,
    "totalBeds" : Housing.total_beds,
    "isCompleted" : Housing.is_completed,
    "incomeClass" : Housing.primary_income_class,
    "zipcode" : Housing.zip_code,
    "projectName" : Housing.project_name
}
@application.route('/api/getAllHousing', methods=['GET'])
@application.route('/api/getAllHousing/', methods=['GET'])
def get_all_housing():
    """
    get stuff from the database
    return stuff from the database
    """
    session = Session()
    query = session.query(Housing)
    params = request.args

    if params:
        projectName = request.args.get('projectName')
        totalUnits = request.args.get('totalUnits')
        totalBeds = request.args.get('totalBeds')
        isCompleted = request.args.get('isCompleted')
        incomeClass = request.args.get('incomeClass')
        zipcode = request.args.get('zipcode')
        sortBy = request.args.get('sortBy')
        ascending = request.args.get('ascending')

        if projectName: query = query.filter(Housing.project_name == projectName)
        if totalUnits: query = query.filter(Housing.total_units == totalUnits)
        if totalBeds: query = query.filter(Housing.total_beds == totalBeds)
        if isCompleted: query = query.filter(Housing.is_completed == isCompleted)
        if incomeClass: query = query.filter(Housing.primary_income_class == incomeClass)
        if zipcode: query = query.filter(Housing.zip_code == zipcode)
        if sortBy and sortBy in housingAttributeMap.keys(): 
            if ascending == "False": 
                if sortBy == "incomeClass":
                    query = query.order_by(
                        case(
                            (Housing.primary_income_class == 'Extremely Low Income Units', 4),
                            (Housing.primary_income_class == 'Very Low Income Units', 3),
                            (Housing.primary_income_class == 'Low Income Units', 2),
                            (Housing.primary_income_class == 'Moderate Income Units', 1),
                            (Housing.primary_income_class == 'Middle Income Units', 0),
                            else_=5
                        )
                    )
                else:
                    query = query.order_by(housingAttributeMap[sortBy].desc())
            else:
                if sortBy == "incomeClass":
                    query = query.order_by(
                        case(
                            (Housing.primary_income_class == 'Extremely Low Income Units', 0),
                            (Housing.primary_income_class == 'Very Low Income Units', 1),
                            (Housing.primary_income_class == 'Low Income Units', 2),
                            (Housing.primary_income_class == 'Moderate Income Units', 3),
                            (Housing.primary_income_class == 'Middle Income Units', 4),
                            else_=5
                        )
                    )
                else:
                    query = query.order_by(housingAttributeMap[sortBy].asc())

    results = query.all()
    
    results_dict = [result.__dict__ for result in results]
    for item in results_dict:
        item.pop("_sa_instance_state", None)
    # return response as json
    session.close()
    return jsonify(results_dict)

@application.route('/api/getHousing/<int:housing_id>', methods = ['GET'])
@application.route('/api/getHousing/<int:housing_id>/', methods = ['GET'])
def get_housing(housing_id):
    session = Session()
    # use the housing_id passed in by user to fetch housing with matching unique identifier
    # result = session.query(HousingLinks) #session.query(Housing, HousingLinks).filter(Housing.id == housing_id).first()
    
    #get the housing data AND the links
    result = session.query(Housing, HousingLinks).join(HousingLinks, HousingLinks.id == Housing.id).filter(Housing.id == housing_id).first() 


    if result:
        # Convert the result to a dictionary
        result_as_dict = {**result[0].__dict__, **result[1].__dict__}
        # Remove the _sa_instance_state attribute
        result_as_dict.pop('_sa_instance_state', None)
        result_as_dict.pop('IGNORE', None)
        session.close()
        # Return the result as JSON
        return jsonify(result_as_dict)
    else:
        session.close()
        return jsonify({'message': 'Entry not found'}), 404

@application.route('/api/searchHousing/<string:prompt>', methods=['GET'])  
def search_housing(prompt):
    session = Session()

    search_query = text(
        "MATCH (project_name, full_address, primary_income_class) AGAINST (:prompt)"
    ).bindparams(prompt=prompt)

    results = session.query(Housing).filter(search_query).all()

    results_dict = [result.__dict__ for result in results]
    for item in results_dict:
        item.pop("_sa_instance_state", None)
    # return response as json
    session.close()
    return jsonify(results_dict)


# Clinic API
clinicAttributeMap = {
    "facility_name" : Clinic.facility_name,
    "zipcode" : Clinic.fac_zip,
    "county" : Clinic.county,
    "forProfit" : Clinic.ownership_type,
    "website" : Clinic.web_site,
    "phone" : Clinic.fac_phone
}

@application.route('/api/getAllClinics', methods=['GET'])
@application.route('/api/getAllClinics/', methods=['GET'])
def get_all_clinics():
    """
    get stuff from the database
    return stuff from the database
    """
    session = Session()
    query = session.query(Clinic)
    params = request.args

    if params:
        facilityName = request.args.get('facility_name')
        zipcode = request.args.get('zipcode')
        county = request.args.get('county')
        forProfit = request.args.get('forProfit')
        website = request.args.get('website')
        phone = request.args.get('phone')
        sortBy = request.args.get('sortBy')
        ascending = request.args.get('ascending')

        if facilityName: query = query.filter(Clinic.facility_name == facilityName)
        if zipcode: query = query.filter(Clinic.fac_zip == zipcode)
        if county: query = query.filter(Clinic.county == county)
        if forProfit: query = query.filter(Clinic.ownership_type == forProfit)
        if website: query = query.filter(Clinic.web_site == website)
        if phone: query = query.filter(Clinic.fac_phone == phone)
        if sortBy and sortBy in clinicAttributeMap.keys(): 
            if ascending == "False": 
                query = query.order_by(clinicAttributeMap[sortBy].desc())
            else:
                query = query.order_by(clinicAttributeMap[sortBy].asc())

    results = query.all()
    results_dict = [result.__dict__ for result in results]
    for item in results_dict:
        item.pop("_sa_instance_state", None)
    session.close()
    # return response as json
    return jsonify(results_dict)

@application.route('/api/getClinic/<int:clinic_id>', methods = ['GET'])
@application.route('/api/getClinic/<int:clinic_id>/', methods = ['GET'])
def get_clinic(clinic_id):
    # use the clinic_id passed in by user to fetch clinic with matching unique identifier
    session = Session()
    result = session.query(Clinic, ClinicLinks).join(ClinicLinks, ClinicLinks.id == Clinic.id).filter(Clinic.id == clinic_id).first() 


    if result:
        # Convert the result to a dictionary
        result_as_dict = {**result[0].__dict__, **result[1].__dict__}
        
        # Remove the _sa_instance_state attribute
        result_as_dict.pop('_sa_instance_state', None)
        result_as_dict.pop('IGNORE', None)

        session.close()
        # Return the result as JSON
        return jsonify(result_as_dict)
    else:
        session.close()
        return jsonify({'message': 'Entry not found'}), 404
    
@application.route('/api/searchClinic/<string:prompt>', methods=['GET'])  
def search_clinic(prompt):
    session = Session()

    # the columns we're searching (facility_name, full_address, ...) must match the FText Index in the table
    search_query = text(
        "MATCH (facility_name, full_address, fac_phone, county, ownership_type) AGAINST (:prompt)"
    ).bindparams(prompt=prompt)

    results = session.query(Clinic).filter(search_query).all()

    results_dict = [result.__dict__ for result in results]
    for item in results_dict:
        item.pop("_sa_instance_state", None)
    # return response as json
    session.close()
    return jsonify(results_dict)

    

# Benefit Programs/Centers API
programAttributeMap = {
    "borough" : Program.borough,
    "zipcode" : Program.zip_code,
    "type" : Program.type,
    "nta" : Program.nta,
    "bbl" : Program.bbl,
    "bin" : Program.bin,
    "censusTract" : Program.census_tract,
    "communityBoard" : Program.community_board,
    "communityCouncil" : Program.community_council,
    "programName" : Program.program_name
}
@application.route('/api/getAllPrograms', methods=['GET'])
@application.route('/api/getAllPrograms/', methods=['GET'])
def get_all_programs():
    """
    get stuff from the database
    return stuff from the database
    """
    session = Session()
    query = session.query(Program)
    params = request.args

    if params:
        borough = request.args.get('borough')
        zipcode = request.args.get('zipcode')
        type = request.args.get('type')
        nta = request.args.get('nta')
        bbl = request.args.get('bbl')
        bin = request.args.get('bin')
        censusTract = request.args.get('censusTract')
        communityBoard = request.args.get('communityBoard')
        communityCouncil = request.args.get('communityCouncil')
        sortBy = request.args.get('sortBy')
        ascending = request.args.get('ascending')
        programName = request.args.get('programName')

        if programName: query = query.filter(Program.program_name == programName)
        if borough: query = query.filter(Program.borough == borough)
        if zipcode: query = query.filter(Program.zip_code == zipcode) 
        if type: query = query.filter(Program.type == type)
        if nta: query = query.filter(Program.nta == nta) 
        if bbl: query = query.filter(Program.bbl == bbl)
        if bin: query = query.filter(Program.bin == bin) 
        if censusTract: query = query.filter(Program.census_tract == censusTract)
        if communityBoard: query = query.filter(Program.community_board == communityBoard) 
        if communityCouncil: query = query.filter(Program.community_council == communityCouncil) 

        if sortBy and sortBy in programAttributeMap.keys(): 
            if ascending == "False": 
                query = query.order_by(programAttributeMap[sortBy].desc())
            else:
                query = query.order_by(programAttributeMap[sortBy].asc())

    results = query.all()
    results_dict = [result.__dict__ for result in results]
    for item in results_dict:
        item.pop("_sa_instance_state", None)
    # return response as json
    session.close()
    return jsonify(results_dict)

@application.route('/api/getProgram/<int:program_id>', methods = ['GET'])
@application.route('/api/getProgram/<int:program_id>/', methods = ['GET'])
def get_program(program_id):
    # use the program_id passed in by user to fetch program with matching unique identifier
    # Query the database using the provided ID
    session = Session()
    result = session.query(Program, ProgramLinks).join(ProgramLinks, ProgramLinks.id == Program.id).filter(Program.id == program_id).first() 

    if result:
        # Convert the result to a dictionary
        result_as_dict = {**result[0].__dict__, **result[1].__dict__}
        # Remove the _sa_instance_state attribute
        result_as_dict.pop('_sa_instance_state', None)
        result_as_dict.pop('IGNORE', None)

        session.close()
        # Return the result as JSON
        return jsonify(result_as_dict)
    else:
        session.close()
        return jsonify({'message': 'Entry not found'}), 404

@application.route('/api/searchProgram/<string:prompt>', methods=['GET'])  
def search_program(prompt):
    session = Session()

    search_query = text(
        "MATCH (program_name, type, address, borough, phone, nta, comments) AGAINST (:prompt)"
    ).bindparams(prompt=prompt)

    results = session.query(Program).filter(search_query).all()

    results_dict = [result.__dict__ for result in results]
    for item in results_dict:
        item.pop("_sa_instance_state", None)
    # return response as json
    session.close()
    return jsonify(results_dict)


if __name__ == '__main__':
    # application.register_blueprint(clinics_blueprint)
    # application.register_blueprint(programs_blueprint)
    # application.register_blueprint(housing_blueprint)
    application.run(debug=True)