"""
This file takes the following four json files:
    1. benefits-access.json
    https://data.cityofnewyork.us/Business/Directory-of-Benefits-Access-Centers/9d9t-bmk7
        borough, program_name, street-address, city, state
        zip_code, phone_number_s, latitude, longitude, community_board
        community_council, census_tract, bin, bbl, nta, comments

    2. homebase.json
    https://data.cityofnewyork.us/Social-Services/Directory-Of-Homebase-Locations/ntcm-2w4k
        homebase_office, service_area_zip_code, address, phone_number
        postcode, borough, latitude, longitude, community_board
        council_district, census_tract, bin, bbl, nta

    3. homeless-drop-in.json
    https://data.cityofnewyork.us/Social-Services/Directory-Of-Homeless-Drop-In-Centers/bmxf-3rd4
        program_name, borough, address, comments, postcode
        latitude, longitude, community_board, council_district
        census_tract, bin, bbl, nta

    4. snap-kitchens.json
    https://data.cityofnewyork.us/Social-Services/Directory-of-SNAP-Centers/tc6u-8rnp
        borough, program_name, street_address, city, zip_code
        state, phone)number_s_, latitude, longitude, community_board
        communinty_council, census_tract, bin, bbl, nta, comments

    5. youth.json
    https://data.cityofnewyork.us/Social-Services/DYCD-after-school-programs-Runaway-And-Homeless-Yo/ujsc-un6m
        program_type, program, program_name, borough_community, agency
        contact_number, postcode,location_1, number_and_street_address, 
        latitude, longitude, community_board, community_council, census_tract
        bin, bbl, nta

        
and combines them all into 1 new giant json: programs-new.json.

First, we need to filter each database's data to get the intersection
of all of their attributes.

    Intersection:
        1. borough
        2. program_name
        3. address
        4. city
        5. state
        6. zip_code
        7. phone

    json1:
    borough, program_name, street_address, city, state
    zip_code, phone_number_s, latitude, longitude, community_board
    community_council, census_tract, bin, bbl, nta, comments

    json2:
    borough, program_name, address, comments, postcode
    latitude, longitude, community_board, council_district
    census_tract, bin, bbl, nta

    json3:
    program_name, borough, address, comments, postcode
    latitude, longitude, community_board, council_district
    census_tract, bin, bbl, nta

    json4:
    borough, program_name, street_address, city, zip_code
    state, phone_number_s_, latitude, longitude, community_board
    communinty_council, census_tract, bin, bbl, nta, comments

    json5:
    borough_community, program_name, number_and_street_address, program_type, program,
    contact_number, postcode, location_1,
    latitude, longitude, community_board, community_council, census_tract
    bin, bbl, nta

    intersection:
    borough, program_name, address, zip_code, phon3, latitude, longitude, 
    community_board, census_tract, bin, bbl, nta, comments, type
        nta = neighborhood tabulation area
        bin = building identificationn number
        bbl = borough block lot
"""

import pandas as pd

df_access = pd.read_json('src/api/scraped/benefits-access.json')
df_homebase = pd.read_json('src/api/scraped/homebase.json')
df_drop_in = pd.read_json('src/api/scraped/homeless-drop-in.json')
df_snap = pd.read_json('src/api/scraped/snap-kitchens.json')
df_youth = pd.read_json('src/api/scraped/youth.json')

df_access.rename(columns={'street_address':'address', 'phone_number_s':'phone'}, inplace=True)
df_access.drop(columns=['city', 'state'], inplace=True)
df_access['type'] = "Benefits Access Center"
df_access.reset_index(drop=True, inplace=True)

df_homebase.rename(columns={'postcode': 'zip_code', 'council_district':'community_council', 'homebase_office':'program_name', 'phone_number':'phone'}, inplace=True)
df_homebase.drop(columns=['service_area_zip_code'], inplace=True)
df_homebase['type'] = "Homebase (Homeless Prevention Network) Office"
df_homebase['comments'] = "None"
df_homebase.reset_index(drop=True, inplace=True)

df_drop_in.rename(columns={'postcode':'zip_code', 'council_district':'community_council'}, inplace=True)
df_drop_in['type'] = "Homeless Drop-in Centers"
df_drop_in['phone'] = "None"
df_drop_in.reset_index(drop=True, inplace=True)

df_snap.rename(columns={'street_address':'address', 'phone_number_s_':'phone'}, inplace=True)
df_snap.drop(columns=['city', 'state'], inplace=True)
df_snap['type'] = "SNAP Center"
df_snap.reset_index(drop=True, inplace=True)

def concatenate(row):
    name = row['program_name']
    program = row['program']
    if program is None: program = "Program"

    return name + ' (' + program + ')'
df_youth['new_program_name'] = df_youth.apply(concatenate, axis=1)
df_youth.drop(columns=['program_name', 'program_type', 'program', 'agency', 'location_1', ":@computed_region_efsh_h5xi",  ":@computed_region_f5dn_yrer", ":@computed_region_yeji_bk3q", ":@computed_region_92fq_4b7q", ":@computed_region_sbqj_enih"], inplace=True)
df_youth.rename(columns={'new_program_name':'program_name', 'borough_community':'borough', 'number_and_street_address':'address', 'contact_number': 'phone', 'postcode':'zip_code'}, inplace=True)
df_youth['type'] = "Runaway & Homeless Youth"
df_youth['comments'] = "None"
df_youth.reset_index(drop=True, inplace=True)


intersection = set(df_access.columns) & set(df_homebase.columns) & set(df_drop_in.columns) & set(df_snap.columns) & set(df_youth.columns)
combined_df = pd.concat([df_access, df_homebase, df_drop_in, df_snap, df_youth], ignore_index=False)
# keep the first occurrence, mark others as duplicates
duplicates = combined_df['address'].duplicated(keep='first')  

filtered_df = combined_df[~duplicates]


filtered_df.to_json('src/api/scraped/all-programs.json', orient='records',indent=2)