from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Float, Text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

"""
Each data model is a Python class representing a SQL tabl in our database
Attributes of a model translate to columns in a table.
"""
class Clinic(Base):
    __tablename__ = "clinics"
    id = Column(Integer, primary_key=True, autoincrement="auto")
    facility_name = Column(String(255))
    full_address = Column(String(255), unique=False)
    fac_phone = Column(String(255))
    fac_zip = Column(Integer)
    web_site = Column(String(255))
    latitude = Column(Float)
    longitude = Column(Float)
    county = Column(String(255))
    ownership_type = Column(String(255))
    # housing_inst1 = Column(Integer, ForeignKey('housing.id')) 
    # housing_inst2 = Column(Integer, ForeignKey('housing.id'))
    # housing_inst3 = Column(Integer, ForeignKey('housing.id'))
    # program_inst1 = Column(Integer, ForeignKey('programs.id')) 
    # program_inst2 = Column(Integer, ForeignKey('programs.id'))
    # program_inst3 = Column(Integer, ForeignKey('programs.id'))
    image = Column(String(100))

    def __repr__(self):
        return f"<Clinic {self.id}, {self.facility_name}, {self.full_address}>"
    
class ClinicLinks(Base):
    __tablename__ = "cliniclinks"
    IGNORE = Column(Integer, primary_key=True, autoincrement="auto") #SQLAlchemy requires at least 1 primary key
    id = Column(Integer, ForeignKey('clinics.id'))
    housing_inst1 = Column(Integer, ForeignKey('housing.id'))
    housing_inst2 = Column(Integer, ForeignKey('housing.id'))
    housing_inst3 = Column(Integer, ForeignKey('housing.id'))
    program_inst1 = Column(Integer, ForeignKey('programs.id')) 
    program_inst2 = Column(Integer, ForeignKey('programs.id'))
    program_inst3 = Column(Integer, ForeignKey('programs.id'))

    def __repr__(self):
        return f"<HousingLinks {self.id, self.housing_inst1,self.housing_inst2,self.housing_inst3,self.program_inst1,self.program_inst2,self.program_inst3 }>"

class Housing(Base):
    __tablename__ = "housing"
    id = Column(Integer, primary_key=True, autoincrement="auto")
    project_id = Column(Integer)
    project_name = Column(String(255))
    full_address = Column(String(255), unique=False)
    # zip_code = Column(Integer)
    total_units = Column(Integer)
    total_beds = Column(Integer)
    is_completed = Column(Boolean)
    primary_income_class = Column(String(255))
    latitude = Column(Float)
    longitude = Column(Float)
    # clinic_inst1 = Column(Integer, ForeignKey('clinics.id'))
    # clinic_inst2 = Column(Integer, ForeignKey('clinics.id'))
    # clinic_inst3 = Column(Integer, ForeignKey('clinics.id'))
    # program_inst1 = Column(Integer, ForeignKey('programs.id')) 
    # program_inst2 = Column(Integer, ForeignKey('programs.id'))
    # program_inst3 = Column(Integer, ForeignKey('programs.id'))
    image = Column(String(100))
    zip_code = Column(Integer)

    def __repr__(self):
        return f"<Housing {self.id, self.project_name, {self.full_address}}>"
    
class HousingLinks(Base):
    __tablename__ = "housinglinks"
    IGNORE = Column(Integer, primary_key=True, autoincrement="auto") #SQLAlchemy requires at least 1 primary key
    id = Column(Integer, ForeignKey('housing.id'))
    clinic_inst1 = Column(Integer, ForeignKey('clinics.id'))
    clinic_inst2 = Column(Integer, ForeignKey('clinics.id'))
    clinic_inst3 = Column(Integer, ForeignKey('clinics.id'))
    program_inst1 = Column(Integer, ForeignKey('programs.id')) 
    program_inst2 = Column(Integer, ForeignKey('programs.id'))
    program_inst3 = Column(Integer, ForeignKey('programs.id'))

    def __repr__(self):
        return f"<HousingLinks {self.id, self.clinic_inst1,self.clinic_inst2,self.clinic_inst3,self.program_inst1,self.program_inst2,self.program_inst3 }>"


class Program(Base):
    __tablename__ = "programs"
    id = Column(Integer, primary_key=True, autoincrement="auto")
    program_name = Column(String(255))
    type = Column(String(255))
    address = Column(String(255), unique=False)
    borough = Column(String(255))
    zip_code = Column(Integer)
    phone = Column(String(255))
    latitude = Column(Float)
    longitude = Column(Float)
    community_board = Column(Integer)
    community_council = Column(Integer)
    census_tract = Column(Integer)
    bin = Column(Integer)
    bbl = Column (Integer)
    nta = Column(String(255))
    comments = Column(String(255))
    # clinic_inst1 = Column(Integer, ForeignKey('clinics.id'))
    # clinic_inst2 = Column(Integer, ForeignKey('clinics.id'))
    # clinic_inst3 = Column(Integer, ForeignKey('clinics.id'))
    # housing_inst1 = Column(Integer, ForeignKey('housing.id')) 
    # housing_inst2 = Column(Integer, ForeignKey('housing.id'))
    # housing_inst3 = Column(Integer, ForeignKey('housing.id'))
    image = Column(String(100))

    def __repr__(self):
        return f"<Program {self.id}, {self.program_name}, {self.type}>" 

class ProgramLinks(Base):
    __tablename__ = "programlinks"
    IGNORE = Column(Integer, primary_key=True, autoincrement="auto") #SQLAlchemy requires at least 1 primary key
    id = Column(Integer, ForeignKey('programs.id'))
    clinic_inst1 = Column(Integer, ForeignKey('clinics.id'))
    clinic_inst2 = Column(Integer, ForeignKey('clinics.id'))
    clinic_inst3 = Column(Integer, ForeignKey('clinics.id'))
    housing_inst1 = Column(Integer, ForeignKey('housing.id')) 
    housing_inst2 = Column(Integer, ForeignKey('housing.id'))
    housing_inst3 = Column(Integer, ForeignKey('housing.id'))

    def __repr__(self):
        return f"<ProgramLinks {self.id, self.clinic_inst1,self.clinic_inst2,self.clinic_inst3,self.housing_inst1,self.housing_inst2,self.housing_inst3 }>"
    

