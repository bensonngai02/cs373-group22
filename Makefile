install:
	npm install

run:
	echo "Starting the application on your local server..."
	npm start
	echo "Application is running!"

build:
	echo "Building your application for deployment..."
	npm run build
	echo "Build complete!"

test:
	echo "Running tests..."
	npm test
	echo "Tests run!"

clean:
	echo "Cleaning build artifacts..."
	rm -rf build/
	rm -rf node_modules/

# Pull files from branch you're currently on
pull:
	make clean
	@echo
	git pull
	git status