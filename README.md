### New York Aid

# Canvas / Ed Discussion group number
Group 22

# Names of the team members
* Kapil Rampalli
* Angelo Culotta
* Benson Ngai
* Osarumwende Elaiho
* Sashank Meka

# Team Leader
Benson Ngai

# Phase Leaders
The phase leaders responsibilities are to organize and lead regular zoom meetings, and also regularly check in on
everyone's progress to ensure that we meet deadlines. They are also responsible for delegating remaining tasks
and creating short term goals for all other team members.
 
Phase I - Benson Ngai
Phase II - Benson Ngai 
Phase III - Angelo Culotta / Kapil Rampalli
Phase IV - Osaru Elaiho / Sashank Meka

# Latest Git SHA
05e6dd13bce930178c6c9d8b6da387a8ff5907e3

# Name of the project (alphanumeric, no spaces, max 32 chars; this will also be your URL)
New-York-Aid

# Website Link
www.new-york-aid.me

# Backend API Link
https://api.new-york-aid.me/api/
https://new-york-aid.me/api/

# API Docs Link
https://documenter.getpostman.com/view/25932782/2s9YJXYjvs#a662fbe2-ae89-4d41-b4fe-c6286cbc21c4

# Estimated Project Work Time
Phase I - 7 days
Phase II - 12 days
Phase III - 7 days
Phase IV - 3 days

# Actual Project Work Time
Phase I - 5-6 days
Phase II - 12-13 days
Phase III - 5-6 days
Phase IV - 2 days

# Proposed project
In recent years, New York City has experienced the highest level of homelessness in the United States, with approximately 85,000 homeless people in June 2023. Our project, New York Aid, is an online platform committed to make a difference in the lives of NYC’s homeless population. It will provide comprehensive information, such as healthcare clinics, low-cost housing, and benefits programs, to serve as a beacon of hope and support for those in need. Ultimately, New York Aid is dedicated to fostering positive change and extending a helping hand to those who need it most in the Big Apple.

# URLs of at least three data sources that you will programmatically scrape using a RESTful API (be very sure about this)
## Affordable Housing
* https://data.cityofnewyork.us/Housing-Development/Affordable-Housing-Production-by-Building/hg8x-zxpr
* https://dev.socrata.com/foundry/data.cityofnewyork.us/hg8x-zxpr

## Benefits Programs
* https://data.cityofnewyork.us/Social-Services/NYC-Benefits-Platform-Benefits-and-Programs-Datase/kvhd-5fmu
* https://dev.socrata.com/foundry/data.cityofnewyork.us/kvhd-5fmu
* https://data.cityofnewyork.us/Social-Services/Directory-of-SNAP-Centers/tc6u-8rnp

## Healthcare Clinics/Medical Centers
* https://dev.socrata.com/foundry/data.cityofnewyork.us/kvhd-5fmu
* https://data.cityofnewyork.us/Business/Directory-of-Benefits-Access-Centers/9d9t-bmk7
* https://data.cityofnewyork.us/Social-Services/Directory-Of-Homebase-Locations/ntcm-2w4k
* https://data.cityofnewyork.us/Social-Services/Directory-Of-Homeless-Drop-In-Centers/bmxf-3rd4
* https://data.cityofnewyork.us/Social-Services/Directory-of-SNAP-Centers/tc6u-8rnp
* https://data.cityofnewyork.us/Social-Services/DYCD-after-school-programs-Runaway-And-Homeless-Yo/ujsc-un6m


# At least three models
1. Healthcare Clinics
2. Low-cost housing
3. Benefits programs

# Estimate of the number of instances of each model
* Healthcare Clinics: ~250
* Benefits Programs: ~100
* Housing: ~1000

# Each model must have many attributes
## Healthcare Clinics
* Facility Name
* Address
* Phone
* Zip code
* Website
* County
* Profit/Non-profit

## Low-cost housing
* Total available units
* Number of rooms
* Max people per unit
* Price
* Location
* Distance to current location
* Contact (person/phone number to get in contact with the inquire about housing)
* Restrictions (pets, etc.)

## Benefits programs
* Program Name
* Type
* Address
* Borough
* Zip Code
* Phone
* Latitude
* Longitude
* Community Board
* Community Council
* Census Tract
* Building Identification Number
* Borough Block Lot
* Neighborhood Tabulation Area
* Comments

# Describe 5 of those attributes for each model that you can filter or sort
## Healthcare Clinics
* Filter by city/zip code (borough)
* Sort by alphabetical order of healthcare center name
* Filter by profit/nonprofit
* Increasing/decreasing order of distance from current location
* Sort by facility opening date (how old/new a facility is)

## Low-cost housing
* Borough
* Housing within distance from address
* Price range (Low/med/extremely low income)
* Project Name
* Building age

## Benefits programs
* Sort by alphabetical order of Program Name
* Filter by benefits programs type
* Filter by zip code
* Sort by neighborhood tabulation area
* Increasing order of distance from current location

<!-- Instances of each model must connect to instances of at least two other models
Each type of model can connect to nearby instances of the other models
Instances of each model must be rich with different media (e.g., feeds, images, maps, text, videos, etc.) (be very sure about this) -->

# Describe 2 types of media for instances of each model
## Healthcare Clinics
* Text with description of healthcare center
* Image of the address pertaining to particular model (from Google Maps Static Api - street view )
* Map of the address pertaining to particular model
## Low-cost housing
* Text with description of house 
* Image of the address pertaining to particular model (from Google Maps Static Api - street view )
* Map of the address pertaining to particular model
## Benefit Programs
* Text as description of benefit program
* Map of the address pertaining to particular model
* Image of logo of benefit program

# Describe 3 questions that your site will answer
1. Where are healthcare clinics/medical centers/substance abuse centers that I can seek out for resources along with their contacts/hours of operation?
2. What are benefits programs that I can enroll as a homeless person and what benefits would I receive? What programs are near me?
3. Where can I find low-cost housing, and how can I stay there?
